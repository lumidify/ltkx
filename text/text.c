#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "stb_truetype.h"
#include <graphite2/Segment.h>

/* These unicode routines are taken from
 * https://github.com/JeffBezanson/cutef8 */

/* is c the start of a utf8 sequence? */
#define isutf(c) (((c)&0xC0)!=0x80)

static const uint32_t offsetsFromUTF8[6] = {
    0x00000000UL, 0x00003080UL, 0x000E2080UL,
    0x03C82080UL, 0xFA082080UL, 0x82082080UL
};

/* next character without NUL character terminator */
uint32_t u8_nextmemchar(const char *s, size_t *i)
{
    uint32_t ch = 0;
    size_t sz = 0;
    do {
        ch <<= 6;
        ch += (unsigned char)s[(*i)++];
        sz++;
    } while (!isutf(s[*i]));
    ch -= offsetsFromUTF8[sz-1];

    return ch;
}


#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h" /* http://nothings.org/stb/stb_truetype.h */

stbtt_fontinfo ltk_load_font(const char *path)
{
	FILE *f;
	long len;
	char *contents;
	stbtt_fontinfo info;
	f = fopen(path, "rb");
	fseek(f, 0, SEEK_END);
	len = ftell(f);
	fseek(f, 0, SEEK_SET);
	contents = malloc(len + 1);
	fread(contents, 1, len, f);
	contents[len] = '\0';
	fclose(f);
	if (!stbtt_InitFont(&info, contents, 0))
	{
		fprintf(stderr, "Failed to load font %s\n", path);
		exit(1);
	}
	return info;
}

int ltk_text_width(uint8_t *text, stbtt_fontinfo fontinfo, int height)
{
	float scale = stbtt_ScaleForPixelHeight(&fontinfo, height);
	size_t i = 0;
	int length = strlen(text);
	if (length < 1) return 0;
	int temp_x;
	int kern_advance;
	int width = 0;
	uint32_t char1;
	uint32_t char2;
	char1 = u8_nextmemchar(text, &i);
	while(i <= length)
	{
		stbtt_GetCodepointHMetrics(&fontinfo, char1, &temp_x, 0);
		width += temp_x * scale;

		char2 = u8_nextmemchar(text, &i);
		if (!char2) break;
		kern_advance = stbtt_GetCodepointKernAdvance(&fontinfo, char1, char2);
		width += kern_advance * scale;
		char1 = char2;
	}

	return width;
}

Pixmap ltk_render_text(
	Display *display,
	Window window,
	GC gc,
	uint8_t *text,
	stbtt_fontinfo fontinfo,
	int height,
	unsigned long fg,
	unsigned long bg,
	const char *font_
)
{
        int rtl = 1; /*are we rendering right to left? probably not */
        int pointsize = 24; /*point size in points */
        int dpi = 96; /*work with this many dots per inch */
        char *pError; /*location of faulty utf-8 */
        gr_font *font = NULL;
        size_t numCodePoints = 0;
        gr_segment *seg = NULL;
        const gr_slot *s;
        gr_face *face = gr_make_file_face(font_, 0);
        if (!face) return 1;
        //font = gr_make_font(pointsize * dpi / 72.0f, face);
        font = gr_make_font(pointsize * dpi / 72.0f, face);
        if (!font) return 2;
        numCodePoints = gr_count_unicode_characters(gr_utf8, text, NULL, (const void **)(&pError));
        if (pError) return 3;
        seg = gr_make_seg(font, face, 0, 0, gr_utf8, text, numCodePoints, rtl);
        if (!seg) return 3;
        for (s = gr_seg_first_slot(seg); s; s = gr_slot_next_in_segment(s))
                printf("%d(%f,%f) ", gr_slot_gid(s), gr_slot_origin_X(s), gr_slot_origin_Y(s));
	//printf("\n%f\n", gr_seg_advance_X(seg));

	XWindowAttributes attrs;
	XGetWindowAttributes(display, window, &attrs);
	int depth = attrs.depth;

//	int width = ltk_text_width(text, fontinfo, height);
	int width = (int)gr_seg_advance_X(seg) + 100;
	unsigned char *bitmap = calloc(sizeof(char), width * (height + 200));
	//float scale = stbtt_ScaleForPixelHeight(&fontinfo, pointsize * dpi / 72.0f);
	float scale = stbtt_ScaleForMappingEmToPixels(&fontinfo, pointsize * dpi / 72.0f);

	int ascent, descent, line_gap;
	stbtt_GetFontVMetrics(&fontinfo, &ascent, &descent, &line_gap);
	ascent *= scale;
	descent *= scale;

	size_t i = 0;
	int length = strlen(text);
	if (length < 1)
	{
		printf("WARNING: ltk_render_text: length of text is less than 1.\n");
		return XCreatePixmap(display, window, 0, 0, depth);
	}
	uint32_t char1, char2;
	char1 = u8_nextmemchar(text, &i);
	int ax, lsb, x = 0, y = 0, x1, y1, x2, y2, byte_offset, kern_advance;
	int w, h, xoff, yoff;
	int y_abs = 0;
//	while (i <= length)
	unsigned char *b;
        for (s = gr_seg_first_slot(seg); s; s = gr_slot_next_in_segment(s))
	{
		stbtt_GetGlyphBitmapBox(&fontinfo, gr_slot_gid(s), scale, scale, &x1, &y1, &x2, &y2);
		//stbtt_MakeGlyphBitmap(&fontinfo, bitmap + byte_offset, x2 - x1, y2 - y1, width, scale, scale, gr_slot_gid(s));
                b = stbtt_GetGlyphBitmap(&fontinfo, scale, scale, gr_slot_gid(s), &w, &h, &xoff, &yoff);
		x = (int)(50 + xoff + gr_slot_origin_X(s));
		y = (int)(200 + yoff - gr_slot_origin_Y(s));
                for (int i = 0; i < h; i++)
                {
                        for (int j = 0; j < w; j++)
                        {
				byte_offset = (y + i) * width + x + j;
                                bitmap[byte_offset] = bitmap[byte_offset] + b[i * w + j];
                                if (bitmap[byte_offset] > 255) bitmap[byte_offset] = 255;
                        }
                }
                free(b);
	}
        gr_seg_destroy(seg);
        gr_font_destroy(font);
        gr_face_destroy(face);

	/* TODO: separate this into a separate function so that one function only creates
	 * the bitmap and other functions to turn that into a pixmap with solid color
	 * background, image background, etc.
	 */
	Pixmap rendered = XCreatePixmap(display, window, width, height + 200, depth);
	XSetForeground(display, gc, bg);
	for (int i = 0; i < (height + 200); i++)
	{
		for (int j = 0; j < width; j++)
		{
			/* Yay! Magic! */
			XSetForeground(display, gc, (bitmap[i * width + j] / 255.0) * fg + (1 - bitmap[i * width + j] / 255.0) * bg);
			XDrawPoint(display, rendered, gc, j, i);
		}
	}
	XSetForeground(display, gc, bg);
	return rendered;
}

int main(int argc, char *argv[])
{
    Display *display;
    int screen;
    Window window;
    GC gc;

    unsigned long black, white;
    Colormap colormap;
    display = XOpenDisplay((char *)0);
    screen = DefaultScreen(display);
    colormap = DefaultColormap(display, screen);
    black = BlackPixel(display, screen);
    white = WhitePixel(display, screen);
    window = XCreateSimpleWindow(display, DefaultRootWindow(display), 0, 0, 1366, 512, 0, black, white);
    XSetStandardProperties(display, window, "Random Window", NULL, None, NULL, 0, NULL);
    XSelectInput(display, window, ExposureMask|ButtonPressMask|KeyPressMask);
    gc = XCreateGC(display, window, 0, 0);
    XSetBackground(display, gc, black);
    XSetForeground(display, gc, black);
    XClearWindow(display, window);
    XMapRaised(display, window);
    XColor c1, c2;
    XParseColor(display, colormap, "#FFFFFF", &c1);
    XParseColor(display, colormap, "#FF0000", &c2);
    XAllocColor(display, colormap, &c1);
    XAllocColor(display, colormap, &c2);

    stbtt_fontinfo fontinfo = ltk_load_font("Awami_beta3.ttf");
    int width = ltk_text_width("ہمارے بارے میںffsafdsfasfasf", fontinfo, 100);
    Pixmap pix = ltk_render_text(display, window, gc, "ہمارے بارے میں", fontinfo, 64, c1.pixel, c2.pixel, "Awami_beta3.ttf");
//    Pixmap pix = ltk_render_text(display, window, gc, "Bob", fontinfo, 64, c1.pixel, c2.pixel, "GentiumPlus-R.ttf");
    XCopyArea(display, pix, window, gc, 0, 0, width, 64, 0, 0);

    XEvent event;
    KeySym key;
    char text[255];

    while(1)
    {
        XNextEvent(display, &event);
        if (event.type == KeyPress && XLookupString(&event.xkey, text, 255, &key, 0) == 1)
        {
            XCopyArea(display, pix, window, gc, 0, 0, width, 300, 0, 0);
            if (text[0] == 'q')
            {
                XFreeGC(display, gc);
                XFreeColormap(display, colormap);
                XDestroyWindow(display, window);
                XCloseDisplay(display);
                exit(0);
            }
        }
    }
    
    return 0;
}
