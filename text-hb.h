/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2017, 2018, 2020 lumidify <nobody@lumidify.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef TEXT_HB_H
#define TEXT_HB_H

#include <harfbuzz/hb.h>
#include <harfbuzz/hb-ot.h>
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h" /* http://nothings.org/stb/stb_truetype.h */
#include "khash.h"
#include <X11/Xlib.h>
#include <fontconfig/fontconfig.h>
#include <fribidi.h>

typedef struct {
	stbtt_fontinfo info;
	hb_font_t *hb;
	uint16_t id;
	unsigned int refs;
} LtkFont;

/* Contains general info on glyphs that doesn't change regardless of the context */
typedef struct _LtkGlyphInfo {
	unsigned int id;
	unsigned char *alphamap;
	unsigned int w;
	unsigned int h;
	unsigned int xoff; /* x offset from origin to top left corner of glyph */
	unsigned int yoff; /* y offset from origin to top left corner of glyph */
	unsigned int refs;
	/* FIXME: does refs need to be long? It could cause problems if a
	program tries to cache/"keep alive" a lot of pages of text. */
} LtkGlyphInfo;

/* Contains glyph info specific to one run of text */
typedef struct _LtkGlyph {
	LtkGlyphInfo *info;
	int x_offset; /* additional x offset given by harfbuzz */
	int y_offset; /* additional y offset given by harfbuzz */
	int x_advance;
	int y_advance;
	int x_abs;
	int y_abs;
	uint32_t cluster; /* index of char in original text - from harfbuzz */
	struct _LtkGlyph *next;
} LtkGlyph;

/* Single segment of text with same font */
typedef struct LtkTextSegment {
	uint16_t font_id;
	uint16_t font_size;
	unsigned int w;
	unsigned int h;
	int start_x;
	int start_y;
	int x_min;
	int y_min;
	int x_max;
	int y_max;
	hb_direction_t dir;
	uint32_t *str;
	LtkGlyph *start_glyph;
	struct LtkTextSegment *next;
} LtkTextSegment;

/* Single line of text */
typedef struct {
	unsigned int w;
	unsigned int h;
	int x_max;
	int x_min;
	int y_max;
	int y_min;
	FriBidiParType dir;
	LtkTextSegment *start_segment;
} LtkTextLine;

/* Hash definitions */
/* glyph id -> glyph info struct */
KHASH_MAP_INIT_INT(glyphinfo, LtkGlyphInfo*)
/* font path, size -> glyph cache hash */
KHASH_MAP_INIT_INT(glyphcache, khash_t(glyphinfo)*)
/* font path -> font id */
KHASH_MAP_INIT_STR(fontid, uint16_t)
/* font id -> font struct */
KHASH_MAP_INIT_INT(fontstruct, LtkFont*)

typedef struct LtkTextManager_ {
	khash_t(fontid) *font_paths;
	khash_t(fontstruct) *font_cache;
	khash_t(glyphcache) *glyph_cache;
	FcPattern *fcpattern;
	uint16_t default_font;
	uint16_t font_id_cur;
} LtkTextManager;

LtkTextManager *ltk_init_text(char *font_name);

void ltk_destroy_text_manager(LtkTextManager *tm);

LtkGlyphInfo *ltk_create_glyph_info(LtkFont *font, unsigned int id, float scale);

void ltk_destroy_glyph_info(LtkGlyphInfo *gi);

LtkGlyphInfo *ltk_get_glyph_info(LtkFont *font, unsigned int id, float scale, khash_t(glyphinfo) *cache);

khint_t ltk_create_glyph_cache(LtkTextManager *tm, uint16_t font_id, uint16_t font_size);

void ltk_destroy_glyph_cache(khash_t(glyphinfo) *cache);

void ltk_load_default_font(LtkTextManager *tm, char *name);

LtkFont *ltk_create_font(char *path, uint16_t id);

void ltk_destroy_font(LtkFont *font);

/* FIXME: need to figure out how exactly the whole font system is going to work, especially with default fonts, etc. */
uint16_t ltk_load_font(LtkTextManager *tm, char *path);

uint16_t ltk_get_font(LtkTextManager *tm, char *path);

/* TODO: different sizes, colors, styles, etc. */
LtkTextLine *ltk_create_text_line(LtkTextManager *tm, char *text, uint16_t fontid, uint16_t size);

/* FIXME: could use unsigned int for fontid and size as long as there is code to check neither of them become too large
   -> in case I want to get rid of uint_16_t, etc. */
LtkTextSegment *ltk_create_text_segment(LtkTextManager *tm, uint32_t *text, unsigned int text_len, uint16_t fontid, uint16_t size, hb_script_t script);

void ltk_destroy_glyph(LtkGlyph *glyph, khash_t(glyphinfo) *cache);

void ltk_destroy_text_segment(LtkTextSegment *ts);

XImage *ltk_render_text_line(LtkTextLine *tl, Display *dpy, Window window, GC gc, Colormap colormap, XColor fg, XColor bg);

void ltk_render_text_segment(LtkTextSegment *ts, unsigned int start_x, unsigned int start_y, XImage *img, XColor fg);


#endif
