/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016, 2017 Lumidify Productions <lumidify@openmailbox.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "text.h"

uint32_t u8_nextmemchar(const char *s, size_t *i)
{
    uint32_t ch = 0;
    size_t sz = 0;
    do {
        ch <<= 6;
        ch += (unsigned char)s[(*i)++];
        sz++;
    } while (!isutf(s[*i]));
    ch -= offsetsFromUTF8[sz-1];

    return ch;
}

stbtt_fontinfo ltk_load_font(const char *path)
{
	FILE *f;
	long len;
	char *contents;
	stbtt_fontinfo info;
	f = fopen(path, "rb");
	fseek(f, 0, SEEK_END);
	len = ftell(f);
	fseek(f, 0, SEEK_SET);
	contents = malloc(len + 1);
	fread(contents, 1, len, f);
	contents[len] = '\0';
	fclose(f);
	if (!stbtt_InitFont(&info, contents, 0))
	{
		fprintf(stderr, "Failed to load font %s\n", path);
		exit(1);
	}
	return info;
}

int ltk_text_width(uint8_t *text, stbtt_fontinfo fontinfo, int height)
{
	float scale = stbtt_ScaleForPixelHeight(&fontinfo, height);
	size_t i = 0;
	int length = strlen(text);
	if (length < 1) return 0;
	int temp_x;
	int kern_advance;
	int width = 0;
	uint32_t char1;
	uint32_t char2;
	char1 = u8_nextmemchar(text, &i);
	while(i <= length)
	{
		stbtt_GetCodepointHMetrics(&fontinfo, char1, &temp_x, 0);
		width += temp_x * scale;

		char2 = u8_nextmemchar(text, &i);
		if (!char2) break;
		kern_advance = stbtt_GetCodepointKernAdvance(&fontinfo, char1, char2);
		width += kern_advance * scale;
		char1 = char2;
	}

	return width;
}

unsigned char *ltk_render_text(uint8_t *text, stbtt_fontinfo fontinfo, int height, int width)
{
/*	int width = ltk_text_width(text, fontinfo, height);*/
	unsigned char *bitmap = calloc(width * height, sizeof(char));
	float scale = stbtt_ScaleForPixelHeight(&fontinfo, height);

	int ascent, descent, line_gap;
	stbtt_GetFontVMetrics(&fontinfo, &ascent, &descent, &line_gap);
	ascent *= scale;
	descent *= scale;

	size_t i = 0;
	int length = strlen(text);
	if (length < 1)
	{
		printf("WARNING: ltk_render_text: length of text is less than 1.\n");
		return bitmap;
	}
	uint32_t char1, char2;
	char1 = u8_nextmemchar(text, &i);
	int ax, x = 0, y, x1, y1, x2, y2, byte_offset, kern_advance;
	while (i <= length)
	{
		stbtt_GetCodepointBitmapBox(&fontinfo, char1, scale, scale, &x1, &y1, &x2, &y2);
		y = ascent + y1;
		byte_offset = x + (y  * width);
		stbtt_MakeCodepointBitmap(&fontinfo, bitmap + byte_offset, x2 - x1, y2 - y1, width, scale, scale, char1);

		stbtt_GetCodepointHMetrics(&fontinfo, char1, &ax, 0);
		x += ax * scale;

		char2 = u8_nextmemchar(text, &i);
		if (!char2) break;
		kern_advance = stbtt_GetCodepointKernAdvance(&fontinfo, char1, char2);
		x += kern_advance * scale;
		char1 = char2;
	}

	return bitmap;
}
