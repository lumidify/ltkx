/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016, 2017, 2018 lumidify <nobody@lumidify.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "ltk.h"
#include "text-hb.h"
#include "button.h"

void ltk_button_ini_handler(LtkTheme *theme, const char *prop, const char *value)
{
	if (strcmp(prop, "border_width") == 0) {
		theme->button->border_width = atoi(value);
	} else if (strcmp(prop, "font_size") == 0) {
		theme->button->font_size = atoi(value);
	} else if (strcmp(prop, "pad") == 0) {
		theme->button->pad = atoi(value);
	} else if (strcmp(prop, "border") == 0) {
		theme->button->border = ltk_create_xcolor(value);
	} else if (strcmp(prop, "fill") == 0) {
		theme->button->fill = ltk_create_xcolor(value);
	} else if (strcmp(prop, "border_hover") == 0) {
		theme->button->border_hover = ltk_create_xcolor(value);
	} else if (strcmp(prop, "fill_hover") == 0) {
		theme->button->fill_hover = ltk_create_xcolor(value);
	} else if (strcmp(prop, "border_pressed") == 0) {
		theme->button->border_pressed = ltk_create_xcolor(value);
	} else if (strcmp(prop, "fill_pressed") == 0) {
		theme->button->fill_pressed = ltk_create_xcolor(value);
	} else if (strcmp(prop, "border_active") == 0) {
		theme->button->border_active = ltk_create_xcolor(value);
	} else if (strcmp(prop, "fill_active") == 0) {
		theme->button->fill_active = ltk_create_xcolor(value);
	} else if (strcmp(prop, "border_disabled") == 0) {
		theme->button->border_disabled = ltk_create_xcolor(value);
	} else if (strcmp(prop, "fill_disabled") == 0) {
		theme->button->fill_disabled = ltk_create_xcolor(value);
	} else if (strcmp(prop, "text_color") == 0) {
		theme->button->text_color = ltk_create_xcolor(value);
	} else {
		printf("WARNING: Unknown property \"%s\" for button style.\n");
	}
}

void ltk_draw_button(LtkButton *button)
{
	LtkButtonTheme *theme = ltk_global->theme->button;
	LtkWindow *window = button->widget.window;
	LtkRect rect = button->widget.rect;
	int bw = theme->border_width;
	XColor border;
	XColor fill;
	XImage *img;
	int text_x, text_y;
	switch (button->widget.state) {
	case LTK_NORMAL:
		border = theme->border;
		fill = theme->fill;
		img = button->text;
		break;
	case LTK_HOVERACTIVE:
	case LTK_HOVER:
		border = theme->border_hover;
		fill = theme->fill_hover;
		img = button->text_hover;
		break;
	case LTK_PRESSED:
		border = theme->border_pressed;
		fill = theme->fill_pressed;
		img = button->text_pressed;
		break;
	case LTK_ACTIVE:
		border = theme->border_active;
		fill = theme->fill_active;
		img = button->text_active;
		break;
	case LTK_DISABLED:
		border = theme->border_disabled;
		fill = theme->fill_disabled;
		img = button->text_disabled;
		break;
	default:
		ltk_fatal("No style found for button!\n");
	}
	XSetForeground(ltk_global->display, window->gc, fill.pixel);
	XFillRectangle(ltk_global->display, window->xwindow, window->gc, rect.x, rect.y, rect.w, rect.h);
	/* FIXME: Why did I do this? */
	if (bw < 1) return;
	XSetForeground(ltk_global->display, window->gc, border.pixel);
	XSetLineAttributes(ltk_global->display, window->gc, bw, LineSolid, CapButt, JoinMiter);
	XDrawRectangle(ltk_global->display, window->xwindow, window->gc, rect.x + bw / 2, rect.y + bw / 2, rect.w - bw, rect.h - bw);
	if (!img) {
		img = ltk_render_text_line(button->tl, ltk_global->display, window->xwindow, window->gc, ltk_global->colormap, theme->text_color, fill);
		/* FIXME: any nicer way to do this? */
		switch (button->widget.state) {
			case LTK_NORMAL:
				button->text = img;
				break;
			case LTK_HOVERACTIVE:
			case LTK_HOVER:
				button->text_hover = img;
				break;
			case LTK_PRESSED:
				button->text_pressed = img;
				break;
			case LTK_ACTIVE:
				button->text_active = img;
				break;
			case LTK_DISABLED:
				button->text_disabled = img;
				break;
		}
	}
	text_x = rect.x + (rect.w - button->tl->w) / 2;
	text_y = rect.y + (rect.h - button->tl->h) / 2;
	XPutImage(ltk_global->display, window->xwindow, window->gc, img, 0, 0, text_x, text_y, button->tl->w, button->tl->h);
}

LtkButton *ltk_create_button(LtkWindow *window, const char *text, void (*callback) (void))
{
	LtkButton *button = malloc(sizeof(LtkButton));

	if (button == NULL) {
		ltk_fatal("ERROR: Unable to allocate memory for LtkButton.\n");
	}

	button->widget = ltk_create_widget(window, &ltk_draw_button, &ltk_destroy_button, 1);
	button->widget.mouse_release = &ltk_button_mouse_release;

	button->callback = callback;
	LtkTheme *theme = ltk_global->theme;
	button->tl = ltk_create_text_line(ltk_global->tm, text, ltk_global->tm->default_font, theme->button->font_size);
	button->widget.rect.w = button->tl->w + (theme->button->border_width + theme->button->pad) * 2;
	button->widget.rect.h = button->tl->h + (theme->button->border_width + theme->button->pad) * 2;
	button->text = NULL;
	button->text_pressed = NULL;
	button->text_hover = NULL;
	button->text_disabled = NULL;
	button->text_active = NULL;

	return button;
}

void ltk_destroy_button(void *widget)
{
	LtkButton *button = (LtkButton *) widget;
	if (!button) {
		printf("WARNING: Tried to destroy NULL button.\n");
	}
	if (button->text) XDestroyImage(button->text);
	if (button->text_hover) XDestroyImage(button->text_hover);
	if (button->text_pressed) XDestroyImage(button->text_pressed);
	if (button->text_active) XDestroyImage(button->text_active);
	if (button->text_disabled) XDestroyImage(button->text_disabled);
	//ltk_destroy_text_segment(button->ts);
	free(button);
}

/* FIXME: is the fixme below supposed to be for the function above? */
/* FIXME: ungrid button if gridded */
void ltk_button_mouse_release(void *widget, XEvent event)
{
	LtkButton *button = widget;
	if (button->widget.state == LTK_HOVERACTIVE && button->callback) {
		button->callback();
	}
}
