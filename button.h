/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016, 2017, 2018 lumidify <nobody@lumidify.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef _LTK_BUTTON_H_
#define _LTK_BUTTON_H_

#include "text-hb.h"

typedef struct {
	LtkWidget widget;
	void (*callback) (void);
	LtkTextLine *tl;
	XImage *text;
	XImage *text_hover;
	XImage *text_pressed;
	XImage *text_active;
	XImage *text_disabled;
} LtkButton;

typedef struct LtkButtonTheme {
	int border_width;
	int font_size;
	XColor text_color;
	int pad;

	XColor border;
	XColor fill;

	XColor border_hover;
	XColor fill_hover;

	XColor border_pressed;
	XColor fill_pressed;

	XColor border_active;
	XColor fill_active;

	XColor border_disabled;
	XColor fill_disabled;
} LtkButtonTheme;

void ltk_draw_button(LtkButton * button);

LtkButton *ltk_create_button(LtkWindow * window, const char *text, void (*callback) (void));

void ltk_destroy_button(void *widget);

void ltk_button_mouse_release(void *widget, XEvent event);

#endif
