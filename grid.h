/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016, 2017, 2018 lumidify <nobody@lumidify.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef _LTK_GRID_H_
#define _LTK_GRID_H_

#include "ltk.h"

/*
 * Struct to represent a grid widget.
 */
typedef struct LtkGrid {
	LtkWidget widget;
	unsigned int rows;
	unsigned int columns;
	void **widget_grid;
	unsigned int *row_heights;
	unsigned int *column_widths;
	unsigned int *row_weights;
	unsigned int *column_weights;
	unsigned int *row_pos;
	unsigned int *column_pos;
} LtkGrid;

/*
 * Set the weight of a row in a grid.
 * grid: The grid.
 * row: The row.
 * weight: The weight to set the row to.
 */
void ltk_set_row_weight(LtkGrid * grid, int row, int weight);

/*
 * Set the weight of a column in a grid.
 * grid: The grid.
 * column: The column.
 * weight: The weight to set the row to.
 */
void ltk_set_column_weight(LtkGrid * grid, int column, int weight);

/*
 * Draw all the widgets in a grid.
 * grid: The grid to draw the widgets of.
 */
void ltk_draw_grid(LtkGrid * grid);

/*
 * Create a grid.
 * window: The window the grid will displayed on.
 * rows: The number of rows in the grid.
 * columns: The number of columns in the grid.
 */
LtkGrid *ltk_create_grid(LtkWindow * window, int rows, int columns);

/*
 * Destroy a grid.
 * widget: Pointer to the grid.
 */
void ltk_destroy_grid(void *widget);

/*
 * Recalculate the positions and dimensions of the
 * columns, rows, and widgets in a grid.
 * widget: Pointer to the grid.
 */
void ltk_recalculate_grid(void *widget);

/*
 * Grid a widget.
 * ptr: Pointer to the widget.
 * grid: The grid.
 * row: The row to grid the widget in.
 * column: The column to grid the widget in.
 * rowspan: The amount of rows the widget should span.
 * columnspan: The amount of columns the widget should span.
 * sticky: Mask of the sticky values (LTK_STICKY_*).
 */
void ltk_grid_widget(void *ptr, LtkGrid * grid, int row, int column,
		     int rowspan, int columnspan, unsigned short sticky);

/*
 * Delegate a mouse press event on the grid to the proper widget.
 * widget: The grid.
 * event: The event to be handled.
 */
void ltk_grid_mouse_press(void *widget, XEvent event);

/*
 * Delegate a mouse release event on the grid to the proper widget.
 * widget: The grid.
 * event: The event to be handled.
 */
void ltk_grid_mouse_release(void *widget, XEvent event);

/*
 * Delegate a mouse motion event on the grid to the proper widget.
 * widget: The grid.
 * event: The event to be handled.
 */
void ltk_grid_motion_notify(void *widget, XEvent event);

#endif
