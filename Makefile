LIBS = -lm `pkg-config --libs x11 harfbuzz fontconfig fribidi`
STD = -std=c99
FLAGS = -g -w -fcommon -Wall -Werror -Wextra `pkg-config --cflags x11 harfbuzz fontconfig fribidi`#-pedantic
CFILES = text-hb.c ltk.c ini.c grid.c button.c test1.c

all: test1.c
	gcc $(STD) $(FLAGS) $(LIBS) $(CFILES) -o test
