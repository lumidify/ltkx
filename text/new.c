#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#include <harfbuzz/hb.h>
#include <harfbuzz/hb-ot.h>

char *ltk_load_file(const char *path, size_t *len)
{
        FILE *f;
        char *contents;
        f = fopen(path, "rb");
        fseek(f, 0, SEEK_END);
        *len = ftell(f);
        fseek(f, 0, SEEK_SET);
        contents = malloc(*len + 1);
        fread(contents, 1, *len, f);
        contents[*len] = '\0';
        fclose(f);
        return contents;
}

int main(int argc, char *argv[])
{
   stbtt_fontinfo font;
   hb_blob_t *blob;
   hb_face_t *face;
   size_t filelen = 0;
   char *filedata = ltk_load_file("NotoNastaliqUrdu-Regular.ttf", &filelen);

   if (filedata == 0) exit(1);
   if (!stbtt_InitFont(&font, filedata, stbtt_GetFontOffsetForIndex(filedata,0))) exit(1);

   int height = 32;
   int font_scale = stbtt_ScaleForPixelHeight(&font, height);

   blob = hb_blob_create((const char *)filedata, (unsigned int)filelen, HB_MEMORY_MODE_READONLY, NULL, NULL);
   if (!blob) exit(1);
   face = hb_face_create(blob, 0);
   if (!face) exit(1);
   hb_blob_destroy(blob); // face keeps its ref.

   hb_font_t *hb_font = hb_font_create(face);
   if (!hb_font) exit(1);
   hb_face_destroy(face); // font keeps ref

   // stb_truetype only does TrueType/OpenType which HB supports natively
   hb_ot_font_set_funcs(hb_font);

   int x = 0;
   int y = 0;
   const char *msg = "ہمارے بارے میں";

   hb_buffer_t *buf;
   hb_glyph_info_t *ginf;
   hb_glyph_position_t *gpos;
   unsigned int len = 0;

   buf = hb_buffer_create();
   if (!buf) exit(1);

   // set up the buffer
   hb_buffer_set_flags(buf, HB_BUFFER_FLAG_BOT | HB_BUFFER_FLAG_EOT); // beginning and end of text both
   hb_buffer_add_utf8(buf, msg, -1, 0, -1);

   hb_buffer_guess_segment_properties(buf);

   // shape it
   hb_shape(hb_font, buf, NULL, 0);

   ginf = hb_buffer_get_glyph_infos(buf, &len);
   gpos = hb_buffer_get_glyph_positions(buf, &len);
   if ((!ginf || !gpos) && len) exit(1);

   int width = 0;
   for (int i = 0; i < len; i++) {
	width += gpos[i].x_advance * font_scale;
   }

   unsigned char *bitmap = calloc((width + 100) * (height + 100), sizeof(unsigned char));

        int ascent, descent, line_gap;
        stbtt_GetFontVMetrics(&font, &ascent, &descent, &line_gap);
        ascent *= font_scale;
        descent *= font_scale;

        int ax, x1, y1, x2, y2, byte_offset, kern_advance;
	unsigned char *b;
	for (int i = 0; i < len; i++)
        {
		hb_glyph_info_t *gi = &ginf[i];
		hb_glyph_position_t *gp = &gpos[i];
                stbtt_GetGlyphBitmapBox(&font, gi->codepoint, font_scale, font_scale, &x1, &y1, &x2, &y2);
                y = ascent + y1;
                byte_offset = x + (y  * width);
                b = stbtt_GetGlyphBitmap(&font, font_scale, font_scale, gi->codepoint, &x1, &y1, &x2, &y2);
		int h = y2 - y1;
		int x = x2 - x1;
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
                                bitmap[(yb + i) * (width + 100) + (xb + j)] = bitmap[(yb + i) * (width + 100) + (xb + j)] + b[i * w + j];
				if (bitmap[(yb + i) * (width + 100) + (xb + j)] > 255) bitmap[(yb + i) * (width + 100) + (xb + j)] = 255;
			}
		}
		free(b);

                stbtt_GetGlyphHMetrics(&font, gi->codepoint, &ax, 0);
                x += ax * font_scale;

		if (i < len - 1) {
                kern_advance = stbtt_GetGlyphKernAdvance(&font, gi->codepoint, &ginf[i + 1]->codepoint);
                x += kern_advance * font_scale;
        }

        return bitmap;
   }

   printf("sfsdf\n");
   stbi_write_png("test.png", width + 100, height + 100, 3, bitmap, sizeof(char) * width);
   
   hb_buffer_destroy(buf);
   hb_font_destroy(hb_font);
   return 0;
}
