#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>

int main(int argc, char *argv[])
{
    Display *display;
    int screen;
    Window window;
    GC gc;

    unsigned long black, white;
    XColor green;
    Colormap colormap;
    display = XOpenDisplay((char *)0);
    screen = DefaultScreen(display);
    colormap = DefaultColormap(display, screen);
    black = BlackPixel(display, screen);
    white = WhitePixel(display, screen);
    XParseColor(display, colormap, "#00FF00", &green);
    XAllocColor(display, colormap, &green);
    window = XCreateSimpleWindow(display, DefaultRootWindow(display), 0, 0, 200, 300, 0, white, white);
    XSetStandardProperties(display, window, "Random Window", NULL, None, NULL, 0, NULL);
    XSelectInput(display, window, ExposureMask|ButtonPressMask|KeyPressMask);
    gc = XCreateGC(display, window, 0, 0);
    XSetBackground(display, gc, black);
    XSetForeground(display, gc, black);
    XClearWindow(display, window);
    XMapRaised(display, window);

    XEvent event;
    KeySym key;
    char text[255];



/* this pointer will hold the returned font structure. */
XFontStruct* font_info;

/* try to load the given font. */
char* font_name = "*";
font_info = XLoadQueryFont(display, font_name);
if (!font_info) {
    fprintf(stderr, "XLoadQueryFont: failed loading font '%s'\n", font_name);
}

XSetFont(display, gc, font_info->fid);
/* assume that win_width contains the width of our window, win_height        */
/* contains the height of our window, and 'win' is the handle of our window. */

/* some temporary variables used for the drawing. */
int x, y;

/* draw a "hello world" string on the top-left side of our window. */
x = 0;
y = 0;
XDrawString(display, window, gc, x, y, "hello world", strlen("hello world"));

/* draw a "middle of the road" string in the middle of our window. */
char* text_string = "middle of the road";
/* find the width, in pixels, of the text that will be drawn using */
/* the given font.                                                 */
int string_width = XTextWidth(font_info, text_string, strlen(text_string));
/* find the height of the characters drawn using this font.        */
int font_height = font_info->ascent + font_info->descent;
/*
x = (win_width - string_width) / 2;
y = (win_width - font_height) / 2;
*/
x = 0;
y = 0;
XDrawString(display, window, gc, x, y, "hello world", strlen("hello world"));

    while(1)
    {
        XNextEvent(display, &event);
        if (event.type == KeyPress && XLookupString(&event.xkey, text, 255, &key, 0) == 1)
        {
            if (text[0] == 'q')
            {
XDrawString(display, window, gc, x, y, "hello world", strlen("hello world"));
/*
                XFreeGC(display, gc);
                XFreeColormap(display, colormap);
                XDestroyWindow(display, window);
                XCloseDisplay(display);
                exit(0);
		*/
            }
        }
    }
}
