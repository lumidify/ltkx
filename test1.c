#include "ltk.h"

void bob1(void)
{
	printf("bob\n");
}

void bob2(void *widget, XEvent event)
{
	LtkButton *button = widget;
	if (button->widget.state == LTK_HOVERACTIVE) {
		ltk_quit();
	}
}

int main(int argc, char *argv[])
{
	ltk_init("themes/default.ini");
	LtkWindow *window1 = ltk_create_window("Cool Window!", 0, 0, 500, 500);
/*    LtkWindow *window2 = ltk_create_window("Cool Window!", 0, 0, 500, 500);*/
	LtkGrid *grid1 = ltk_create_grid(window1, 2, 2);
	window1->root_widget = grid1;
	ltk_set_row_weight(grid1, 0, 1);
	ltk_set_row_weight(grid1, 1, 1);
	ltk_set_column_weight(grid1, 0, 1);
	ltk_set_column_weight(grid1, 1, 1);
	/* Test callback functions */
	LtkButton *button1 = ltk_create_button(window1, "I'm a button!", &bob1);
	ltk_grid_widget(button1, grid1, 0, 0, 1, 1, LTK_STICKY_LEFT | LTK_STICKY_RIGHT);
	/* Test manual callback functions */
	LtkButton *button2 = ltk_create_button(window1, "I'm a button!", NULL);
	button2->widget.mouse_release = &bob2;
	ltk_grid_widget(button2, grid1, 0, 1, 1, 1, LTK_STICKY_TOP | LTK_STICKY_BOTTOM);
	LtkButton *button3 = ltk_create_button(window1, "I'm a button!", NULL);
	ltk_grid_widget(button3, grid1, 1, 0, 1, 1, LTK_STICKY_TOP | LTK_STICKY_BOTTOM | LTK_STICKY_RIGHT);
	//LtkButton *button4 = ltk_create_button(window1, "I'm a button!", NULL);
	LtkButton *button4 = ltk_create_button(window1, "ہمارے بارے میں blablabla", NULL);
	ltk_grid_widget(button4, grid1, 1, 1, 1, 1, LTK_STICKY_LEFT | LTK_STICKY_BOTTOM);
	ltk_mainloop();
}
