#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include "stb_truetype.h"
#include <harfbuzz/hb.h>
#include <harfbuzz/hb-ot.h>

/* These unicode routines are taken from
 * https://github.com/JeffBezanson/cutef8 */

/* is c the start of a utf8 sequence? */
#define isutf(c) (((c)&0xC0)!=0x80)

static const uint32_t offsetsFromUTF8[6] = {
    0x00000000UL, 0x00003080UL, 0x000E2080UL,
    0x03C82080UL, 0xFA082080UL, 0x82082080UL
};

/* next character without NUL character terminator */
uint32_t u8_nextmemchar(const char *s, size_t *i)
{
    uint32_t ch = 0;
    size_t sz = 0;
    do {
        ch <<= 6;
        ch += (unsigned char)s[(*i)++];
        sz++;
    } while (!isutf(s[*i]));
    ch -= offsetsFromUTF8[sz-1];

    return ch;
}


#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h" /* http://nothings.org/stb/stb_truetype.h */

stbtt_fontinfo ltk_load_font(const char *path)
{
	FILE *f;
	long len;
	char *contents;
	stbtt_fontinfo info;
	f = fopen(path, "rb");
	fseek(f, 0, SEEK_END);
	len = ftell(f);
	fseek(f, 0, SEEK_SET);
	contents = malloc(len + 1);
	fread(contents, 1, len, f);
	contents[len] = '\0';
	fclose(f);
	if (!stbtt_InitFont(&info, contents, 0))
	{
		fprintf(stderr, "Failed to load font %s\n", path);
		exit(1);
	}
	return info;
}

char *ltk_load_file(const char *path, unsigned long *len)
{
	FILE *f;
	char *contents;
	f = fopen(path, "rb");
	fseek(f, 0, SEEK_END);
	*len = ftell(f);
	fseek(f, 0, SEEK_SET);
	contents = malloc(*len + 1);
	fread(contents, 1, *len, f);
	contents[*len] = '\0';
	fclose(f);
	return contents;
}

int ltk_text_width(uint8_t *text, stbtt_fontinfo fontinfo, int height)
{
	float scale = stbtt_ScaleForMappingEmToPixels(&fontinfo, height);
	size_t i = 0;
	int length = strlen(text);
	if (length < 1) return 0;
	int temp_x;
	int kern_advance;
	int width = 0;
	uint32_t char1;
	uint32_t char2;
	char1 = u8_nextmemchar(text, &i);
	while(i <= length)
	{
		stbtt_GetCodepointHMetrics(&fontinfo, char1, &temp_x, 0);
		width += temp_x * scale;

		char2 = u8_nextmemchar(text, &i);
		if (!char2) break;
		kern_advance = stbtt_GetCodepointKernAdvance(&fontinfo, char1, char2);
		width += kern_advance * scale;
		char1 = char2;
	}

	return width;
}

unsigned long ltk_blend_pixel(Display *display, Colormap colormap, XColor fg, XColor bg, double a)
{
        XColor blended;
        if (a == 1.0)
                return fg.pixel;
        else if (a == 0.0)
                return bg.pixel;
        blended.red = (int)((fg.red - bg.red) * a + bg.red);
        blended.green = (int)((fg.green - bg.green) * a + bg.green);
        blended.blue = (int)((fg.blue - bg.blue) * a + bg.blue);
        XAllocColor(display, colormap, &blended);

        return blended.pixel;
}

Pixmap ltk_render_text(
	Display *display,
	Window window,
	GC gc,
	uint8_t *text,
	stbtt_fontinfo fontinfo,
	int height,
	XColor fg,
	XColor bg,
	const char *font_,
	Colormap colormap
)
{
	hb_blob_t *blob;
	hb_face_t *face;
	size_t filelen = 0;
	char *filedata = ltk_load_file(font_, &filelen);
	blob = hb_blob_create(filedata, filelen, HB_MEMORY_MODE_READONLY, NULL, NULL);
	face = hb_face_create(blob, 0);
	hb_blob_destroy(blob);
	hb_font_t *hbfont = hb_font_create(face);
	hb_face_destroy(face);
	hb_ot_font_set_funcs(hbfont);

	hb_buffer_t *buf;
	hb_glyph_info_t *ginf;
	hb_glyph_position_t *gpos;
	unsigned int len = 0;

	buf = hb_buffer_create();
	hb_buffer_set_flags(buf, HB_BUFFER_FLAG_BOT | HB_BUFFER_FLAG_EOT);
	hb_buffer_add_utf8(buf, text, -1, 0, -1);
	hb_buffer_guess_segment_properties(buf);
	hb_shape(hbfont, buf, NULL, 0);

	ginf = hb_buffer_get_glyph_infos(buf, &len);
	gpos = hb_buffer_get_glyph_positions(buf, &len);

	XWindowAttributes attrs;
	XGetWindowAttributes(display, window, &attrs);
	int depth = attrs.depth;

	int width = ltk_text_width(text, fontinfo, height) + 200;
	float scale = stbtt_ScaleForMappingEmToPixels(&fontinfo, height);

	int ascent, descent, line_gap;
	stbtt_GetFontVMetrics(&fontinfo, &ascent, &descent, &line_gap);
	ascent *= scale;
	descent *= scale;

	Pixmap rendered = XCreatePixmap(display, window, width, height + 200, depth);
	unsigned char *bitmap = calloc(width * (height + 200), sizeof(char));
	int length = strlen(text);
	if (length < 1)
	{
		printf("WARNING: ltk_render_text: length of text is less than 1.\n");
		return XCreatePixmap(display, window, 0, 0, depth);
	}
	int ax, x = 0, y = 0, x1, y1, x2, y2, byte_offset, kern_advance;
	double x_abs = 0, y_abs = 0;
	unsigned char *b;
	int w, h, xoff, yoff;
	for (int i = 0; i < len; i++)
	{
		hb_glyph_info_t *gi = &ginf[i];
		hb_glyph_position_t *gp = &gpos[i];
		stbtt_GetGlyphBitmapBox(&fontinfo, gi->codepoint, scale, scale, &x1, &y1, &x2, &y2);
		x = (int)(x_abs + (gp->x_offset * scale));
		y = (int)((y_abs + 20) + (gp->y_offset * scale));
		printf("%d\n", (int)(gp->y_offset * scale));
		byte_offset = x + (y  * width);
		b = stbtt_GetGlyphBitmap(&fontinfo, scale, scale, gi->codepoint, &w, &h, &xoff, &yoff);

		for (int i = 0; i < h; i++)
		{
			for (int j = 0; j < w; j++)
			{
				bitmap[(y + i) * width + (x + j)] = bitmap[(y + i) * width + (x + j)] + b[i * w + j];
				if (bitmap[(y + i) * width + (x + j)] > 255) bitmap[(y + i) * width + (x + j)] = 255;
			}
		}
		free(b);

		x_abs += (gp->x_advance * scale);
		y_abs -= (gp->y_advance * scale);
	}

                XSetForeground(display, gc, bg.pixel);
                for (int i = 0; i < height + 200; i++)
                {
                        for (int j = 0; j < width; j++)
                        {
                                /* Yay! Magic! */
                                XSetForeground(display, gc, ltk_blend_pixel(display, colormap, fg, bg, (bitmap[i * width + j] / 255.0)));
                                XDrawPoint(display, rendered, gc, j, i);
                        }
                }
                XSetForeground(display, gc, bg.pixel);

	/* TODO: separate this into a separate function so that one function only creates
	 * the bitmap and other functions to turn that into a pixmap with solid color
	 * background, image background, etc.
	 */
	return rendered;
}

int main(int argc, char *argv[])
{
    Display *display;
    int screen;
    Window window;
    GC gc;

    unsigned long black, white;
    Colormap colormap;
    display = XOpenDisplay((char *)0);
    screen = DefaultScreen(display);
    colormap = DefaultColormap(display, screen);
    black = BlackPixel(display, screen);
    white = WhitePixel(display, screen);
    window = XCreateSimpleWindow(display, DefaultRootWindow(display), 0, 0, 1366, 512, 0, black, white);
    XSetStandardProperties(display, window, "Random Window", NULL, None, NULL, 0, NULL);
    XSelectInput(display, window, ExposureMask|ButtonPressMask|KeyPressMask);
    gc = XCreateGC(display, window, 0, 0);
    XSetBackground(display, gc, black);
    XSetForeground(display, gc, black);
    XClearWindow(display, window);
    XMapRaised(display, window);
    XColor c1, c2;
    XParseColor(display, colormap, "#FFFFFF", &c1);
    XParseColor(display, colormap, "#FF0000", &c2);
    XAllocColor(display, colormap, &c1);
    XAllocColor(display, colormap, &c2);

//    stbtt_fontinfo fontinfo = ltk_load_font("GentiumPlus-R.ttf");
    stbtt_fontinfo fontinfo = ltk_load_font("NotoNastaliqUrdu-Regular.ttf");
    int width = ltk_text_width("ہمارے بارے میںdsfasffsfadsf", fontinfo, 200);
    Pixmap pix = ltk_render_text(display, window, gc, "ہمارے بارے میں", fontinfo, 100, c1, c2, "NotoNastaliqUrdu-Regular.ttf", colormap);
//    Pixmap pix = ltk_render_text(display, window, gc, "Bob", fontinfo, 200, c1.pixel, c2.pixel, "GentiumPlus-R.ttf");
    XCopyArea(display, pix, window, gc, 0, 0, width, 200, 0, 0);

    XEvent event;
    KeySym key;
    char text[255];

    while(1)
    {
        XNextEvent(display, &event);
        if (event.type == KeyPress && XLookupString(&event.xkey, text, 255, &key, 0) == 1)
        {
            XCopyArea(display, pix, window, gc, 0, 0, width, 300, 0, 0);
            if (text[0] == 'q')
            {
                XFreeGC(display, gc);
                XFreeColormap(display, colormap);
                XDestroyWindow(display, window);
                XCloseDisplay(display);
                exit(0);
            }
        }
    }
    
    return 0;
}
