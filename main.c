#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>

int main(int argc, char *argv[])
{
	Display *display;
	int screen;
	Window window;
	GC gc;

	unsigned long black, white;
	XColor green;
	Colormap colormap;
	display = XOpenDisplay((char *) 0);
	screen = DefaultScreen(display);
	colormap = DefaultColormap(display, screen);
	black = BlackPixel(display, screen);
	white = WhitePixel(display, screen);
	XParseColor(display, colormap, "#00FF00", &green);
	XAllocColor(display, colormap, &green);
	window =
	    XCreateSimpleWindow(display, DefaultRootWindow(display), 0, 0,
				200, 300, 0, white, green.pixel);
	XSetStandardProperties(display, window, "Random Window", NULL,
			       None, NULL, 0, NULL);
	XSelectInput(display, window,
		     ExposureMask | ButtonPressMask | KeyPressMask);
	gc = XCreateGC(display, window, 0, 0);
	XSetBackground(display, gc, white);
	XSetForeground(display, gc, black);
	XClearWindow(display, window);
	XMapRaised(display, window);

	XEvent event;
	KeySym key;
	char text[255];

	while (1) {
		XNextEvent(display, &event);
		if (event.type == KeyPress
		    && XLookupString(&event.xkey, text, 255, &key,
				     0) == 1) {
			if (text[0] == 'q') {
				XFreeGC(display, gc);
				XFreeColormap(display, colormap);
				XDestroyWindow(display, window);
				XCloseDisplay(display);
				exit(0);
			}
		}
	}
}
