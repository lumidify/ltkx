/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016, 2017, 2018 lumidify <nobody@lumidify.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef _LTK_H_
#define _LTK_H_

#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include "ini.h"
#include "khash.h"
#include "stb_truetype.h"

typedef struct {
	int x;
	int y;
	int w;
	int h;
} LtkRect;

typedef enum {
	LTK_STICKY_LEFT = 1 << 0,
	LTK_STICKY_RIGHT = 1 << 1,
	LTK_STICKY_TOP = 1 << 2,
	LTK_STICKY_BOTTOM = 1 << 3
} LtkStickyMask;

typedef enum {
	LTK_NORMAL = 0,
	LTK_HOVER = 1,
	LTK_PRESSED = 2,
	LTK_ACTIVE = 3,
	LTK_HOVERACTIVE = 4,
	LTK_DISABLED = 5
} LtkWidgetState;

typedef struct LtkWindow LtkWindow;

/* FIXME: change row, column, etc. to void* struct so
   other layout systems can be implemented */
typedef struct LtkWidget {
	LtkWindow *window;
	struct LtkWidget *active_widget;
	struct LtkWidget *hover_widget;
	struct LtkWidget *parent;

	void (*key_press) (void *, XEvent event);
	void (*key_release) (void *, XEvent event);
	void (*mouse_press) (void *, XEvent event);
	void (*mouse_release) (void *, XEvent event);
	void (*motion_notify) (void *, XEvent event);

	void (*resize) (void *);
	void (*draw) (void *);
	void (*destroy) (void *);

	LtkRect rect;
	unsigned int row;
	unsigned int column;
	unsigned int row_span;
	unsigned int column_span;
	unsigned int needs_redraw;
	LtkWidgetState state;
	unsigned short sticky;
} LtkWidget;

/* Window hash */
KHASH_MAP_INIT_INT(winhash, LtkWindow*)

typedef struct LtkWindow {
	Window xwindow;
	GC gc;
	void *root_widget;
	void (*other_event) (void *, XEvent event);
	LtkRect rect;
} LtkWindow;

typedef struct LtkWindowTheme {
	int border_width;
	char *font;
	XColor fg;
	XColor bg;
} LtkWindowTheme;

#include "button.h"
#include "grid.h"

typedef struct {
	LtkWindowTheme *window;
	LtkButtonTheme *button;
} LtkTheme;

LtkTheme *ltk_load_theme(const char *path);

typedef struct {
	LtkTheme *theme;
	LtkTextManager *tm;
	Display *display;
	int screen;
	Colormap colormap;
	khash_t(winhash) *window_hash;
	int window_num;
	Atom wm_delete_msg;
} Ltk;

Ltk *ltk_global;

void ltk_init(const char *theme_path);

void ltk_fatal(const char *msg);

XColor ltk_create_xcolor(const char *hex);

void ltk_mainloop(void);

LtkWindow *ltk_create_window(const char *title, int x, int y,
			     unsigned int w, unsigned int h);

void ltk_redraw_window(LtkWindow * window);

void ltk_remove_window(LtkWindow * window);

void ltk_destroy_window(LtkWindow * window);

void ltk_window_other_event(void *widget, XEvent event);

void ltk_destroy_theme(LtkTheme * theme);

int ltk_collide_rect(LtkRect rect, int x, int y);

char *ltk_read_file(const char *path, unsigned long *len);

void ltk_change_active_widget_state(void *widget, LtkWidgetState state);

void ltk_remove_active_widget(void *widget);

void ltk_remove_hover_widget(void *widget);

LtkWidget ltk_create_widget(LtkWindow * window, void (*draw) (void *),
			    void (*destroy) (void *),
			    unsigned int needs_redraw);

void ltk_mouse_press_event(void *widget, XEvent event);

void ltk_mouse_release_event(void *widget, XEvent event);

void ltk_motion_notify_event(void *widget, XEvent event);

void ltk_handle_event(XEvent event);

#endif
