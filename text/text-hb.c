/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2017, 2018 lumidify <nobody@lumidify.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <harfbuzz/hb.h>
#include <harfbuzz/hb-ot.h>
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h" /* http://nothings.org/stb/stb_truetype.h */
#include "khash.h"

/* TODO: possibly "glyph manager" - only render glyph once and keep info in hash?
   -> would be difficult because of different sizes - would need to keep track of all that.
   -> reference counter - delete glyph from cache if not used anymore - good when there are many ligatures */

/* Font manager: hash for font path -> font id
                 hash for font id -> ltk font struct */

/* FIXME: this needs to be uint32_t, NOT just int! */
/* font path, size -> glyph cache hash */
KHASH_MAP_INIT_INT(ihash, khash_t(iglyph))
/* glyph id -> glyph info struct */
KHASH_MAP_INIT_INT(iglyph, LtkGlyphInfo*)
/* font path -> font struct */
KHASH_MAP_INIT_STR(cfont, LtkFont*)

typedef struct LtkTextManager_ {
	khash_t(cfont) *font_cache;
	khash_t(ihash) *glyph_cache;
	unsigned int font_id_cur;
} LtkTextManager;

/* Make LtkFont union of LtkFontHB, LtkFontGR, and LtkFontLT to handle harfbuzz, graphite, and normal latin? */
typedef struct {
	stbtt_fontinfo info;
	hb_font_t *hb;
	unsigned int id;
} LtkFont;

void
ltk_render_glyph(LtkFont *font, unsigned int id, float scale, khash_t(iglyph) *cache)
{
	int x1, y1, x_off, y_off;
	int ret;
	khiter t;

	LtkGlyphInfo *info = malloc(sizeof(LtkGlyphInfo));
	info->alphamap = stbtt_GetGlyphBitmap(&font->font_info, scale, scale, id, info->w, info->h, info->x_topleft, info->y_topleft);
	k = kh_put(iglyph, cache, info, &ret);
	/* x/y_advance? */
}

LtkTextManager *
ltk_init_text(void)
{
	LtkTextManager *m = malloc(sizeof LtkTextManager);
	if (!m) ltk_fatal("Memory exhausted when trying to create text manager.");
	m->font_cache = kh_init(cfont);
	m->glyph_cache = kh_init(ihash);
	m->font_id_cur = 0;
	return m;
}

void
ltk_create_glyph_cache(LtkTextManager *m, unsigned int font_id, unsigned int font_size)
{
	khash_t(iglyph) *cache = kh_init(iglyph);
	int ret;
	khiter_t k;
	/* I guess I can just ignore ret for now */
	k = kh_put(iglyph, m->glyph_cache, cache, &ret);
}



char *
ltk_load_file(const char *path, unsigned long *len)
{
	FILE *f;
	char *contents;
	f = fopen(path, "rb");
	fseek(f, 0, SEEK_END);
	*len = ftell(f);
	fseek(f, 0, SEEK_SET);
	contents = malloc(*len + 1);
	fread(contents, 1, *len, f);
	contents[*len] = '\0';
	fclose(f);
	return contents;
}

unsigned long
ltk_blend_pixel(Display *display, Colormap colormap, XColor fg, XColor bg, double a)
{
        if (a >= 1.0) {
		return fg.pixel;
        } else if (a == 0.0) {
		return bg.pixel;
	}

        XColor blended;
        blended.red = (int)((fg.red - bg.red) * a + bg.red);
        blended.green = (int)((fg.green - bg.green) * a + bg.green);
        blended.blue = (int)((fg.blue - bg.blue) * a + bg.blue);
        XAllocColor(display, colormap, &blended);

        return blended.pixel;
}

/* Contains general info on glyphs that doesn't change regardless of the context */
typedef struct _LtkGlyphInfo {
	unsigned char *alphamap;
	unsigned int w;
	unsigned int h;
	unsigned int x_topleft; /* x offset from origin to top left corner of glyph */
	unsigned int y_topleft; /* y offset from origin to top left corner of glyph */
} LtkGlyphInfo;

/* Contains glyph info specific to one run of text */
typedef struct _LtkGlyph {
	LtkGlyphInfo *glyph_info;
	/* Does all this need to be stored or are just the top left coordinates needed? */
	unsigned int x; /* top left x coordinate */
	unsigned int y; /* top left y coordinate */
	unsigned int x_offset; /* additional x offset given by harfbuzz */
	unsigned int y_offset; /* additional y offset given by harfbuzz */
	unsigned int x_advance;
	unsigned int y_advance;
	uint32_t cluster; /* index of char in original text - from harfbuzz */
	struct _LtkGlyph *next;
} LtkGlyph;

typedef struct {
	unsigned int width;
	unsigned int height;
	char *str;
	LtkGlyph *start_glyph;
} LtkTextSegment;

LtkFont *
ltk_load_font(char *path, unsigned int id)
{
	long len;
	LtkFont *font = malloc(sizeof(LtkFont));
	if (!font) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}
	char *contents = ltk_load_file(path, &len);
	if (!stbtt_InitFont(&font->font_info, contents, 0))
	{
		fprintf(stderr, "Failed to load font %s\n", path);
		exit(1);
	}
	hb_blob_t *blob = hb_blob_create(contents, len, HB_MEMORY_MODE_READONLY, NULL, NULL);
	hb_face_t *face = hb_face_create(blob, 0);
	hb_blob_destroy(blob);
	font->hb = hb_font_create(face);
	hb_face_destroy(face);
	hb_ot_font_set_funcs(font->hb);
	font->id = id;
	return font;
}

unsigned char *
ltk_render_text_bitmap(
	uint8_t *text,
	LtkFont *font,
	int size,
	int *width,
	int *height)
{
	/* (x1*, y1*): top left corner (relative to origin and absolute)
	   (x2*, y2*): bottom right corner (relative to origin and absolute) */
	int x1, x2, y1, y2, x1_abs, x2_abs, y1_abs, y2_abs, x_abs = 0, y_abs = 0;
	int x_min = INT_MAX, x_max = INT_MIN, y_min = INT_MAX, y_max = INT_MIN;
	int char_w, char_h, x_off, y_off;

	int byte_offset;
	int alpha;
	/* FIXME: Change to uint8_t? */
	unsigned char *bitmap;
	unsigned char *char_bitmap;
	hb_buffer_t *buf;
	hb_glyph_info_t *ginf, *gi;
	hb_glyph_position_t *gpos, *gp;
	unsigned int text_len = 0;
	int text_bytes = strlen(text);
	if (text_bytes < 1) {
		printf("WARNING: ltk_render_text: length of text is less than 1.\n");
		return "";
	}

	buf = hb_buffer_create();
	hb_buffer_set_flags(buf, HB_BUFFER_FLAG_BOT | HB_BUFFER_FLAG_EOT);
	hb_buffer_add_utf8(buf, text, text_bytes, 0, text_bytes);
	hb_buffer_guess_segment_properties(buf);
	hb_shape(font->font, buf, NULL, 0);
	ginf = hb_buffer_get_glyph_infos(buf, &text_len);
	gpos = hb_buffer_get_glyph_positions(buf, &text_len);
	float scale = stbtt_ScaleForMappingEmToPixels(&font->font_info, size);

	/* Calculate size of bitmap */
	for (int i = 0; i < text_len; i++) {
		gi = &ginf[i];
		gp = &gpos[i];
		stbtt_GetGlyphBitmapBox(&font->font_info, gi->codepoint, scale, scale, &x1, &y1, &x2, &y2);
		x1_abs = (int)(x_abs + x1 + gp->x_offset * scale);
		y1_abs = (int)(y_abs + y1 - gp->y_offset * scale);
		x2_abs = x1_abs + (x2 - x1);
		y2_abs = y1_abs + (y2 - y1);
		if (x1_abs < x_min) x_min = x1_abs;
		if (y1_abs < y_min) y_min = y1_abs;
		if (x2_abs > x_max) x_max = x2_abs;
		if (y2_abs > y_max) y_max = y2_abs;
		x_abs += (gp->x_advance * scale);
		y_abs -= (gp->y_advance * scale);
	}
	x_abs = -x_min;
	y_abs = -y_min;
	*width = x_max - x_min;
	*height = y_max - y_min;
	/* FIXME: calloc checks for integer overflow, right? */
	/* FIXME: check if null returned */
	bitmap = calloc(*width * *height, sizeof(char));
	if (!bitmap) {
		fprintf(stderr, "Can't allocate memory for bitmap!\n");
		exit(1);
	}
	for (int i = 0; i < text_len; i++) {
		gi = &ginf[i];
		gp = &gpos[i];
		stbtt_GetGlyphBitmapBox(&font->font_info, gi->codepoint, scale, scale, &x1, &y1, &x2, &y2);
		char_bitmap = stbtt_GetGlyphBitmap(&font->font_info, scale, scale, gi->codepoint, &char_w, &char_h, &x_off, &y_off);

		x1_abs = (int)(x_abs + x1 + gp->x_offset * scale);
		y1_abs = (int)(y_abs + y1 - gp->y_offset * scale);
		for (int k = 0; k < char_h; k++)
		{
			for (int j = 0; j < char_w; j++)
			{
				byte_offset = (y1_abs + k) * *width + x1_abs + j;
				alpha = bitmap[byte_offset] + char_bitmap[k * char_w + j];
				/* Cap at 255 so char doesn't overflow */
				bitmap[byte_offset] = alpha > 255 ? 255 : alpha;
			}
		}
		free(char_bitmap);

		x_abs += gp->x_advance * scale;
		y_abs -= gp->y_advance * scale;
	}
	return bitmap;
}

Pixmap
ltk_render_text(
	Display *dpy,
	Window window,
	GC gc,
	XColor fg,
	XColor bg,
	Colormap colormap,
	unsigned char *bitmap,
	int width,
	int height)
{
	XWindowAttributes attrs;
	XGetWindowAttributes(dpy, window, &attrs);
	int depth = attrs.depth;
	Pixmap pix = XCreatePixmap(dpy, window, width, height, depth);
	XSetForeground(dpy, gc, bg.pixel);
	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			XSetForeground(dpy, gc, ltk_blend_pixel(dpy, colormap, fg, bg, bitmap[i * width + j] / 255.0));
			XDrawPoint(dpy, pix, gc, j, i);
		}
	}
	return pix;
}

int main(int argc, char *argv[])
{
    Display *display;
    int screen;
    Window window;
    GC gc;

    unsigned long black, white;
    Colormap colormap;
    display = XOpenDisplay((char *)0);
    screen = DefaultScreen(display);
    colormap = DefaultColormap(display, screen);
    black = BlackPixel(display, screen);
    white = WhitePixel(display, screen);
    window = XCreateSimpleWindow(display, DefaultRootWindow(display), 0, 0, 1366, 512, 0, black, white);
    XSetStandardProperties(display, window, "Random Window", NULL, None, NULL, 0, NULL);
    XSelectInput(display, window, ExposureMask|ButtonPressMask|KeyPressMask);
    gc = XCreateGC(display, window, 0, 0);
    XSetBackground(display, gc, black);
    XSetForeground(display, gc, black);
    XClearWindow(display, window);
    XMapRaised(display, window);
    XColor c1, c2;
    XParseColor(display, colormap, "#FFFFFF", &c1);
    XParseColor(display, colormap, "#FF0000", &c2);
    XAllocColor(display, colormap, &c1);
    XAllocColor(display, colormap, &c2);

    LtkFont *font = ltk_load_font("NotoNastaliqUrdu-Regular.ttf");
    int w, h;
    unsigned char *bitmap = ltk_render_text_bitmap("ہمارے بارے میں", font, 256, &w, &h);
    Pixmap pix = ltk_render_text(display, window, gc, c1, c2, colormap, bitmap, w, h);
    XCopyArea(display, pix, window, gc, 0, 0, w, h, 0, 0);

    XEvent event;
    KeySym key;
    char text[255];

    while(1)
    {
        XNextEvent(display, &event);
        if (event.type == KeyPress && XLookupString(&event.xkey, text, 255, &key, 0) == 1)
        {
            XCopyArea(display, pix, window, gc, 0, 0, w, h, 0, 0);
            if (text[0] == 'q')
            {
                XFreeGC(display, gc);
                XFreeColormap(display, colormap);
                XDestroyWindow(display, window);
                XCloseDisplay(display);
                exit(0);
            }
        }
    }
    
    return 0;
}
