#include <stdio.h>
#include "khash.h"

KHASH_MAP_INIT_INT(m32, char)
KHASH_MAP_INIT_INT(32hash, khash_t(m32)*)

int main(int argc, char *argv[])
{
        int ret, is_missing;
        khint_t k;
        khash_t(m32) *h = kh_init(m32);
	khash_t(32hash) *h1 = kh_init(32hash);
        k = kh_put(m32, h, 5, &ret);
        if (!ret) kh_del(m32, h, k);
        kh_value(h, k) = 10;

	k = kh_put(32hash, h1, 20, &ret);
        if (!ret) kh_del(32hash, h1, k);
	kh_value(h1, k) = h;

	k = kh_get(32hash, h1, 20);
	khash_t(m32) *bob = kh_value(h1, k);
	k = kh_get(m32, bob, 5);
	printf("%d\n", kh_value(bob, k));

        return 0;
}
