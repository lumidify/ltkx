/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016, 2017 Lumidify Productions <lumidify@openmailbox.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "theme.h"

LtkTheme *ltk_load_theme(const char *path)
{
    char *file_contents = ltk_read_file(path);

    cJSON *json = cJSON_Parse(file_contents);
    if (!json)
    {
        printf("Theme error before: [%s]\n", cJSON_GetErrorPtr());
        return NULL;
    }
    cJSON *button_json = cJSON_GetObjectItem(json, "button");
    if (!button_json)
    {
        printf("Theme error before: [%s]\n", cJSON_GetErrorPtr());
        return NULL;
    }
    cJSON *window_json = cJSON_GetObjectItem(json, "window");
    if (!window_json)
    {
        printf("Theme error before: [%s]\n", cJSON_GetErrorPtr());
        return NULL;
    }

    LtkTheme *theme = malloc(sizeof(LtkTheme));
    theme->button = ltk_parse_button_theme(button_json);
    theme->window = ltk_parse_window_theme(window_json);

    free(file_contents);
    cJSON_Delete(json);

    return theme;
}

void ltk_destroy_theme(LtkTheme *theme)
{
    free(theme->button);
    free(theme->window);
    free(theme);
}

char *ltk_read_file(const char *path)
{
    FILE *f;
    long len;
    char *file_contents;
    f = fopen(path, "rb");
    fseek(f, 0, SEEK_END);
    len = ftell(f);
    fseek(f, 0, SEEK_SET);
    file_contents = malloc(len + 1);
    fread(file_contents, 1, len, f);
    file_contents[len] = '\0';
    fclose(f);

    return file_contents;
}