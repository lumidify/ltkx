# LTK - Lumidify ToolKit

Note: This has moved to [https://lumidify.org/git/ltkx/](https://lumidify.org/git/ltkx/).

This is work in progress. Please do not attempt to actually use any of the code.

## Licenses of Other Libraries Used

[khash](https://github.com/attractivechaos/klib) by Attractive Chaos: [MIT](https://github.com/attractivechaos/klib/blob/31cb0301482a762af0adbfc85dff1632cecc2bb4/khash.h#L3)

[inih](https://github.com/benhoyt/inih) by Ben Hoyt: [New BSD](https://github.com/benhoyt/inih/blob/master/LICENSE.txt)

[stb_truetype](https://github.com/nothings/stb/blob/master/stb_truetype.h) by Sean T. Barrett: [MIT/Public Domain](https://github.com/nothings/stb/blob/e6afb9cbae4064da8c3e69af3ff5c4629579c1d2/stb_truetype.h#L4815)

[cutef8](https://github.com/JeffBezanson/cutef8/) by Jeff Bezanson: [Public Domain](https://github.com/JeffBezanson/cutef8/blob/ce8607864ef59ceef39fc20c9653265f6b91d4bc/utf8.c#L4)

Note: LTK is in no way affiliated with any of the projects listed above.
