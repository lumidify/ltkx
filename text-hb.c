/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2017, 2018, 2020 lumidify <nobody@lumidify.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <fontconfig/fontconfig.h>
#include "ltk.h"
#include "khash.h"
#include "text-hb.h"
#include <fribidi.h>

/* These unicode routines are taken from
 * https://github.com/JeffBezanson/cutef8 */

/* is c the start of a utf8 sequence? */
#define isutf(c) (((c)&0xC0)!=0x80)

static const uint32_t offsetsFromUTF8[6] = {
    0x00000000UL, 0x00003080UL, 0x000E2080UL,
    0x03C82080UL, 0xFA082080UL, 0x82082080UL
};

/* next character without NUL character terminator */
uint32_t u8_nextmemchar(const char *s, size_t *i)
{
    uint32_t ch = 0;
    size_t sz = 0;
    do {
        ch <<= 6;
        ch += (unsigned char)s[(*i)++];
        sz++;
    } while (!isutf(s[*i]));
    ch -= offsetsFromUTF8[sz-1];

    return ch;
}

/* number of characters in NUL-terminated string */
size_t u8_strlen(const char *s)
{
    size_t count = 0;
    size_t i = 0, lasti;

    while (1) {
        lasti = i;
        while (s[i] > 0)
            i++;
        count += (i-lasti);
        if (s[i++]==0) break;
        (void)(isutf(s[++i]) || isutf(s[++i]) || ++i);
        count++;
    }
    return count;
}

size_t u8_wc_toutf8(char *dest, uint32_t ch)
{
    if (ch < 0x80) {
        dest[0] = (char)ch;
        return 1;
    }
    if (ch < 0x800) {
        dest[0] = (ch>>6) | 0xC0;
        dest[1] = (ch & 0x3F) | 0x80;
        return 2;
    }
    if (ch < 0x10000) {
        dest[0] = (ch>>12) | 0xE0;
        dest[1] = ((ch>>6) & 0x3F) | 0x80;
        dest[2] = (ch & 0x3F) | 0x80;
        return 3;
    }
    if (ch < 0x110000) {
        dest[0] = (ch>>18) | 0xF0;
        dest[1] = ((ch>>12) & 0x3F) | 0x80;
        dest[2] = ((ch>>6) & 0x3F) | 0x80;
        dest[3] = (ch & 0x3F) | 0x80;
        return 4;
    }
    return 0;
}

LtkTextManager *
ltk_init_text(char *font_name)
{
	LtkTextManager *tm = malloc(sizeof(LtkTextManager));
	if (!tm) {
		printf("Memory exhausted when trying to create text manager.");
		exit(1);
	}
	tm->font_paths = kh_init(fontid);
	tm->font_cache = kh_init(fontstruct);
	tm->glyph_cache = kh_init(glyphcache);
	tm->font_id_cur = 0;
	ltk_load_default_font(tm, font_name);

	return tm;
}

void
ltk_destroy_text_manager(LtkTextManager *tm)
{
	int k;

	kh_destroy(fontid, tm->font_paths);

	for (k = kh_begin(tm->font_cache); k != kh_end(tm->font_cache); k++) {
		if (kh_exist(tm->font_cache, k)) {
			ltk_destroy_font(kh_value(tm->font_cache, k));
		}
	}
	kh_destroy(fontstruct, tm->font_cache);

	for (k = kh_begin(tm->glyph_cache); k != kh_end(tm->glyph_cache); k++) {
		if (kh_exist(tm->glyph_cache, k)) {
			ltk_destroy_glyph_cache(kh_value(tm->glyph_cache, k));
		}
	}
	kh_destroy(glyphcache, tm->glyph_cache);

	free(tm);
}

LtkGlyphInfo *
ltk_create_glyph_info(LtkFont *font, unsigned int id, float scale)
{
	LtkGlyphInfo *glyph = malloc(sizeof(LtkGlyphInfo));
	if (!glyph) {
		printf("Out of memory!\n");
		exit(1);
	}
	
	glyph->id = id;
	glyph->alphamap = stbtt_GetGlyphBitmap(
		&font->info, scale, scale, id, &glyph->w,
		&glyph->h, &glyph->xoff, &glyph->yoff
	);

	return glyph;
}

void
ltk_destroy_glyph_info(LtkGlyphInfo *gi)
{
	free(gi->alphamap);
	free(gi);
}

LtkGlyphInfo *
ltk_get_glyph_info(LtkFont *font, unsigned int id, float scale, khash_t(glyphinfo) *cache)
{
	int ret;
	khint_t k;
	LtkGlyphInfo *glyph;
	k = kh_get(glyphinfo, cache, id);
	if (k == kh_end(cache)) {
		glyph = ltk_create_glyph_info(font, id, scale);
		glyph->refs = 0;
		/* FIXME: error checking with ret */
		k = kh_put(glyphinfo, cache, id, &ret);
		kh_value(cache, k) = glyph;
	} else {
		glyph = kh_value(cache, k);
		glyph->refs++;
	}

	return glyph;
}

khint_t
ltk_create_glyph_cache(LtkTextManager *tm, uint16_t font_id, uint16_t font_size)
{
	khash_t(glyphinfo) *cache = kh_init(glyphinfo);
	int ret;
	khint_t k;
	/* I guess I can just ignore ret for now */
	k = kh_put(glyphcache, tm->glyph_cache, font_id << 16 + font_size, &ret);
	kh_value(tm->glyph_cache, k) = cache;

	return k;
}

void
ltk_destroy_glyph_cache(khash_t(glyphinfo) *cache)
{
	int k;
	for (k = kh_begin(cache); k != kh_end(cache); k++) {
		if (kh_exist(cache, k)) {
			ltk_destroy_glyph_info(kh_value(cache, k));
		}
	}
	kh_destroy(glyphinfo, cache);
}

void
ltk_load_default_font(LtkTextManager *tm, char *name)
{
	FcPattern *match;
	FcResult result;
	char *file;
	int index;
	uint16_t font;

	tm->fcpattern = FcNameParse(name);
	FcPatternAddString(tm->fcpattern, FC_FONTFORMAT, "truetype");
	FcConfigSubstitute(NULL, tm->fcpattern, FcMatchPattern);
	FcDefaultSubstitute(tm->fcpattern);
	match = FcFontMatch(NULL, tm->fcpattern, &result);

	FcPatternGetString (match, FC_FILE, 0, (FcChar8 **) &file);
	/* FIXME: Why is index never used? This is the index within the font file,
	   so it might be important, although I'm not sure if stb_truetype even
	   supports it */
	FcPatternGetInteger (match, FC_INDEX, 0, &index);

	tm->default_font = ltk_get_font(tm, file);

	FcPatternDestroy (match);
}

LtkFont *
ltk_create_font(char *path, uint16_t id)
{
	long len;
	LtkFont *font = malloc(sizeof(LtkFont));
	if (!font) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}
	char *contents = ltk_read_file(path, &len);
	if (!stbtt_InitFont(&font->info, contents, 0))
	{
		fprintf(stderr, "Failed to load font %s\n", path);
		exit(1);
	}
	/* FIXME: make use of the destroy function (last argument to hb_blob_create - see hb-blob.cc in harfbuzz source) */
	hb_blob_t *blob = hb_blob_create(contents, len, HB_MEMORY_MODE_READONLY, NULL, NULL);
	hb_face_t *face = hb_face_create(blob, 0);
	/* FIXME: need to use destroy function in order for the original file data to be freed? */
	hb_blob_destroy(blob);
	font->hb = hb_font_create(face);
	hb_face_destroy(face);
	hb_ot_font_set_funcs(font->hb);
	font->id = id;
	font->refs = 0;

	return font;
}

void
ltk_destroy_font(LtkFont *font)
{
	free(font->info.data);
	hb_font_destroy(font->hb);
	free(font);
}

uint16_t
ltk_load_font(LtkTextManager *tm, char *path)
{
	LtkFont *font = ltk_create_font(path, tm->font_id_cur++);
	int ret;
	khint_t k;
	/* FIXME: does kh_destroy also free these copied strings properly? */
	char *key = strdup(path);
	k = kh_put(fontid, tm->font_paths, key, &ret);
	kh_value(tm->font_paths, k) = font->id;
	k = kh_put(fontstruct, tm->font_cache, (khint_t) font->id, &ret);
	kh_value(tm->font_cache, k) = font;
	k = kh_get(fontid, tm->font_paths, path);

	return font->id;
}

uint16_t
ltk_get_font(LtkTextManager *tm, char *path)
{
	int ret;
	khint_t k;
	uint16_t id;
	k = kh_get(fontid, tm->font_paths, path);
	if (k == kh_end(tm->font_paths)) {
		id = ltk_load_font(tm, path);
	} else {
		id = kh_value(tm->font_paths, k);
	}

	return id;
}

/* FIXME: allow to either use fribidi for basic shaping and don't use harfbuzz then,
          or just use harfbuzz (then fribidi doesn't need to do any shaping) */
/* FIXME: take baseline into account; save script in LtkTextSegment */
LtkTextLine *
ltk_create_text_line(LtkTextManager *tm, char *text, uint16_t fontid, uint16_t size)
{
	LtkFont *font;
	LtkFont *default_font;
	LtkTextLine *tl = malloc(sizeof(LtkTextLine));
	tl->start_segment = NULL;
	LtkTextSegment *cur_ts = NULL;
	LtkTextSegment *new_ts = NULL;
	uint16_t default_font_id = fontid;
	uint16_t last_font_id = fontid;
	uint16_t cur_font_id = fontid;
	int k = kh_get(fontstruct, tm->font_cache, fontid);
	font = default_font = kh_value(tm->font_cache, k);

	unsigned int ulen = u8_strlen(text);
	FriBidiChar *log_str = malloc(sizeof(FriBidiChar) * ulen);
	size_t inc = 0;
	for (int i = 0; i < ulen; i++) {
		log_str[i] = u8_nextmemchar(text, &inc);
	}
        FriBidiCharType *pbase_dir = malloc(sizeof(FriBidiCharType) * ulen);
        for (int i = 0; i < ulen; i++) {
                pbase_dir[i] = FRIBIDI_TYPE_ON;
        }
	FriBidiChar *vis_str = malloc(sizeof(FriBidiChar) * ulen);
	ulen = fribidi_charset_to_unicode(FRIBIDI_CHAR_SET_UTF8, text, strlen(text), log_str);
	fribidi_log2vis(log_str, ulen, pbase_dir, vis_str, NULL, NULL, NULL);

	hb_unicode_funcs_t *ufuncs = hb_unicode_funcs_get_default();
	hb_script_t cur_script = hb_unicode_script(ufuncs, vis_str[0]);
	hb_script_t last_script = cur_script;
	size_t pos = 0;
	size_t last_pos = 0;
	size_t start_pos = 0;
	uint32_t ch;
	uint32_t gid;
	for (int p = 0; p < ulen; p++) {
		gid = stbtt_FindGlyphIndex(&font->info, vis_str[p]);
		cur_script = hb_unicode_script(ufuncs, vis_str[p]);
		/*
		   Okay, so get this: If I don't ignore HB_SCRIPT_COMMON (as in the lower line), "I'm a button" is split into
		   individual characters (I guess because apostrophe and space are common characters). However, it all seems
		   to display at least decently. If I do ignore HB_SCRIPT_COMMON, it should technically work better since the
		   normal text is kept in one piece. But alas, it is not so! Because spaces don't cause the text to be split
		   anymore, the "blablabla " in the mixed script text is kept together (with the space at the end), which
		   *for some bizarre reason* causes HarfBuzz to display it RTL as "albalbalb". WHAT IS HAPPENING HERE?

		   Update: Yeah, I was just being stupid - I was passing cur_script instead of last_script to
		           ltk_create_text_segment, so that was messing it up...
			   I have to leave that comment there for at least one commit, though.
		*/
		if (!gid || (last_script != cur_script && cur_script != HB_SCRIPT_INHERITED && cur_script != HB_SCRIPT_COMMON)) {
		//if (!gid || (last_script != cur_script && cur_script != HB_SCRIPT_INHERITED && cur_script)) {
			/* This is extremely efficient... */
			FcPattern *pat = FcPatternDuplicate(tm->fcpattern);
			FcPattern *match;
			FcResult result;
			FcPatternAddBool(pat, FC_SCALABLE, 1);
			FcConfigSubstitute(NULL, pat, FcMatchPattern);
			FcDefaultSubstitute(pat);
			FcCharSet *cs = FcCharSetCreate();
			FcCharSetAddChar(cs, vis_str[p]);
			FcPatternAddCharSet(pat, FC_CHARSET, cs);
			match = FcFontMatch(NULL, pat, &result);
			char *file;
			FcPatternGetString(match, FC_FILE, 0, &file);
			last_font_id = cur_font_id;
			cur_font_id = ltk_get_font(tm, file);
			k = kh_get(fontstruct, tm->font_cache, cur_font_id);
			font = kh_value(tm->font_cache, k);
			FcPatternDestroy(match);
			FcPatternDestroy(pat);

			new_ts = ltk_create_text_segment(tm, vis_str + start_pos, p - start_pos, last_font_id, size, last_script);
			// FIXME: error
			if (!new_ts) continue;
			new_ts->next = NULL;
			if (!tl->start_segment) tl->start_segment = new_ts;
			if (cur_ts) cur_ts->next = new_ts;
			cur_ts = new_ts;
			start_pos = p;

			last_script = cur_script;
		}
	}
	new_ts = ltk_create_text_segment(tm, vis_str + start_pos, ulen - start_pos, cur_font_id, size, last_script);
	// FIXME: error if new_ts null
	new_ts->next = NULL;
	if (!tl->start_segment) tl->start_segment = new_ts;
	if (cur_ts) cur_ts->next = new_ts;
	cur_ts = new_ts;

	free(vis_str);
	free(log_str);
	free(pbase_dir);

	/* calculate width of text line
	   NOTE: doesn't work with mixed horizontal and vertical text */
	LtkTextSegment *ts = tl->start_segment;
	int is_hor = HB_DIRECTION_IS_HORIZONTAL(ts->dir);
	tl->y_max = tl->x_max = INT_MIN;
	tl->y_min = tl->x_min = INT_MAX;
	tl->w = tl->h = 0;
	while (ts) {
		if (is_hor) {
			if (tl->y_max < ts->y_max) {
				tl->y_max = ts->y_max;
			}
			if (tl->y_min > ts->y_min) {
				tl->y_min = ts->y_min;
			}
			tl->w += ts->w;
		} else {
			if (tl->x_max < ts->x_max) {
				tl->x_max = ts->x_max;
			}
			if (tl->x_min > ts->x_min) {
				tl->x_min = ts->x_min;
			}
			tl->h += ts->h;
		}
		ts = ts->next;
	}
	if (is_hor) {
		tl->h = tl->y_max - tl->y_min;
	} else {
		tl->w = tl->x_max - tl->x_min;
	}

	return tl;
}

/* FIXME: could use unsigned int for fontid and size as long as there is code to check neither of them become too large
   -> in case I want to get rid of uint_16_t, etc. */
LtkTextSegment *
ltk_create_text_segment(LtkTextManager *tm, uint32_t *text, unsigned int len, uint16_t fontid, uint16_t size, hb_script_t script)
{
	/* (x1*, y1*): top left corner (relative to origin and absolute)
	   (x2*, y2*): bottom right corner (relative to origin and absolute) */
	LtkFont *font;
	khash_t(glyphinfo) *glyph_cache;
	khint_t k;
	char bob[4];
	for (int i = 0; i < len; i++) {
		for (int j = 0; j < 3; j++) {
			bob[j] = '\0';
		}
		u8_wc_toutf8(bob, text[i]);
		printf("%s", bob);
	}
	printf("END\n");

	k = kh_get(fontstruct, tm->font_cache, fontid);
	font = kh_value(tm->font_cache, k);
	/* FIXME: when should refs be increased? maybe only at the end in case
	   it has to return for some other reason (out of memory, etc.) */
	font->refs++;

	uint32_t attr = fontid << 16 + size;
	/* FIXME: turn this into ltk_get_glyph_cache */
	k = kh_get(glyphcache, tm->glyph_cache, attr);
	if (k == kh_end(tm->glyph_cache)) {
		k = ltk_create_glyph_cache(tm, fontid, size);
	}
	glyph_cache = kh_value(tm->glyph_cache, k);

	LtkTextSegment *ts = malloc(sizeof(LtkTextSegment));
	if (!ts) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}
	ts->str = malloc(sizeof(uint32_t) * (len + 1));
	memcpy(ts->str, text, len * sizeof(uint32_t));
	ts->str[len] = '\0';
	ts->font_id = fontid;
	ts->font_size = size;

	hb_buffer_t *buf;
	hb_glyph_info_t *ginf, *gi;
	hb_glyph_position_t *gpos, *gp;
	unsigned int text_len = 0;
	if (len < 1) {
		printf("WARNING: ltk_render_text_segment: length of text is less than 1.\n");
		return NULL;
	}

	buf = hb_buffer_create();
	hb_direction_t dir = hb_script_get_horizontal_direction(script);
	hb_buffer_set_direction(buf, dir);
	hb_buffer_set_script(buf, script);
	//hb_buffer_set_direction(buf, HB_DIRECTION_LTR);
	//hb_buffer_set_flags(buf, HB_BUFFER_FLAG_BOT | HB_BUFFER_FLAG_EOT);
	hb_buffer_add_codepoints(buf, ts->str, len, 0, len);
	//hb_buffer_guess_segment_properties(buf);
	hb_shape(font->hb, buf, NULL, 0);
	ts->dir = hb_buffer_get_direction(buf);
	ginf = hb_buffer_get_glyph_infos(buf, &text_len);
	gpos = hb_buffer_get_glyph_positions(buf, &text_len);
	float scale = stbtt_ScaleForMappingEmToPixels(&font->info, size);
	LtkGlyph *last_glyph = NULL;

	int x_min = INT_MAX, x_max = INT_MIN, y_min = INT_MAX, y_max = INT_MIN;
	int x_abs = 0, y_abs = 0, x1_abs, y1_abs, x2_abs, y2_abs;
	/* magic, do not touch */
	LtkGlyph *glyph;
	for (int i = 0; i < text_len; i++) {
		gi = &ginf[i];
		gp = &gpos[i];
		glyph = malloc(sizeof(LtkGlyph));
		glyph->info = ltk_get_glyph_info(font, gi->codepoint, scale, glyph_cache);
		/* FIXME: round instead of just casting */
		glyph->x_offset = (int)(gp->x_offset * scale);
		glyph->y_offset = (int)(gp->y_offset * scale);
		glyph->x_advance = (int)(gp->x_advance * scale);
		glyph->y_advance = (int)(gp->y_advance * scale);
		glyph->next = NULL;
		if (i == 0) {
			ts->start_glyph = glyph;
		} else {
			last_glyph->next = glyph;
		}
		last_glyph = glyph;

		/* Calculate position in order to determine full size of text segment */
		x1_abs = x_abs + glyph->info->xoff + glyph->x_offset;
		y1_abs = y_abs + glyph->info->yoff - glyph->y_offset;
		/* Okay, wait, so should I check if the script is horizontal, and then add
		   x_advance instead of glyph->info->w? It seems that the glyph width is
		   usually smaller than x_advance, and spaces etc. are completely lost
		   because their glyph width is 0. I have to distinguish between horizontal
		   and vertical scripts, though because to calculate the maximum y position
		   for horizontal scripts, I still need to use the glyph height since
		   y_advance doesn't really do much there. I dunno, at least *something*
		   works now... */
		//x2_abs = x1_abs + glyph->info->w;
		y2_abs = y1_abs + glyph->info->h;
		x2_abs = x1_abs + glyph->x_advance;
		//y2_abs = y1_abs - glyph->y_advance;
		glyph->x_abs = x1_abs;
		glyph->y_abs = y1_abs;
		if (x1_abs < x_min) x_min = x1_abs;
		if (y1_abs < y_min) y_min = y1_abs;
		if (x2_abs > x_max) x_max = x2_abs;
		if (y2_abs > y_max) y_max = y2_abs;
		x_abs += glyph->x_advance;
		y_abs -= glyph->y_advance;
	}
	/* FIXME: what was this supposed to do?
	   I think it was supposed to be the start drawing position, but why would this not be 0?
	   I'm guessing it had something to do with the fact that I need to calculate where the
	   actual top left corner of the glyph is since harfbuzz gives me the origin - maybe I
	   should just store that position directly in LtkGlyph? Is there any need to advance, etc.
	   later on after I've positioned the glyphs? Well, I guess I'll figure that out eventually... */
        ts->start_x = -x_min;
        ts->start_y = -y_min;
	// FIXME: need to somehow save advance so spaces aren't lost
	ts->w = x_max - x_min;
	ts->h = y_max - y_min;
	ts->x_min = x_min;
	ts->y_min = y_min;
	ts->x_max = x_max;
	ts->y_max = y_max;
	return ts;
}

void
ltk_destroy_glyph(LtkGlyph *glyph, khash_t(glyphinfo) *cache)
{
	int k;
	if (--glyph->info->refs < 1) {
		k = kh_get(glyphinfo, cache, glyph->info->id);
		kh_del(glyphinfo, cache, k);
		ltk_destroy_glyph_info(glyph->info);
	}
	free(glyph);
}

void
ltk_destroy_text_segment(LtkTextSegment *ts)
{
	LtkGlyph *glyph, *next_glyph;
	khash_t(glyphinfo) *gcache;
	LtkFont *font;
	int k;
	glyph = ts->start_glyph;
	k = kh_get(glyphinfo, ltk_global->tm->glyph_cache, ts->font_id << 16 + ts->font_size);
	gcache = kh_value(ltk_global->tm->glyph_cache, k);
	do {
		next_glyph = glyph->next;
		ltk_destroy_glyph(glyph, gcache);
	} while (glyph = next_glyph);
	k = kh_get(fontstruct, ltk_global->tm->font_cache, ts->font_id);
	font = kh_value(ltk_global->tm->font_cache, k);
	if (--font->refs < 1) {
		kh_del(fontstruct, ltk_global->tm->font_cache, k);
		ltk_destroy_font(font);
	}
	free(ts->str);
	free(ts);
}

/* based on http://codemadness.org/git/dwm-font/file/drw.c.html#l315 */
XImage *
ltk_render_text_line(
	LtkTextLine *tl,
	Display *dpy,
	Window window,
	GC gc,
	Colormap colormap,
	XColor fg,
	XColor bg)
{
	XWindowAttributes attrs;
	XGetWindowAttributes(dpy, window, &attrs);
	int depth = attrs.depth;
	XImage *img = XCreateImage(dpy, CopyFromParent, depth, ZPixmap, 0, NULL, tl->w, tl->h, 32, 0);
	img->data = calloc(img->bytes_per_line, img->height);
	XInitImage(img);
	int b;
	for (int i = 0; i < tl->h; i++) {
		b = img->bytes_per_line * i;
		for (int j = 0; j < tl->w; j++) {
			img->data[b++] = bg.blue / 257;
			img->data[b++] = bg.green / 257;
			img->data[b++] = bg.red / 257;
			b++;
		}
	}

	LtkTextSegment *ts = tl->start_segment;
	int x = 0;
	int y = 0;
	int is_hor = HB_DIRECTION_IS_HORIZONTAL(ts->dir);
	do {
		if (is_hor) {
			y = tl->h - tl->y_max;
			ltk_render_text_segment(ts, x + ts->start_x, y, img, fg);
			x += ts->w;
		} else {
			x = tl->w - tl->x_max;
			ltk_render_text_segment(ts, x, y + ts->start_y, img, fg);
			y += ts->h;
		}
	} while (ts = ts->next);

	return img;
}

void
ltk_render_text_segment(
	LtkTextSegment *ts,
	unsigned int start_x,
	unsigned int start_y,
	XImage *img,
	XColor fg)
{
	LtkGlyph *glyph = ts->start_glyph;
	int x_cur = start_x;
	int y_cur = start_y;
	int x, y;
	double a;
	int b;
	do {
		x = x_cur + glyph->info->xoff + glyph->x_offset;
		y = y_cur + glyph->info->yoff - glyph->y_offset;
		for (int i = 0; i < glyph->info->h; i++) {
			for (int j = 0; j < glyph->info->w; j++) {
				b = (y + i) * img->bytes_per_line + (x + j) * 4;
				a = glyph->info->alphamap[i * glyph->info->w + j] / 255.0;
				img->data[b] = (fg.blue * a + (1 - a) * (uint16_t)img->data[b] * 257) / 257;
				img->data[b + 1] = (fg.green * a + (1 - a) * (uint16_t)img->data[b + 1] * 257) / 257;
				img->data[b + 2] = (fg.red * a + (1 - a) * (uint16_t)img->data[b + 2] * 257) / 257;
			}
		}
		x_cur += glyph->x_advance;
		y_cur -= glyph->y_advance;
	} while (glyph = glyph->next);
}
