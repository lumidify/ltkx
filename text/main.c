/* By Justin Meiners (2013) */
/* font and image utilties from nothings.org */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>

/* is c the start of a utf8 sequence? */
#define isutf(c) (((c)&0xC0)!=0x80)

static const uint32_t offsetsFromUTF8[6] = {
    0x00000000UL, 0x00003080UL, 0x000E2080UL,
    0x03C82080UL, 0xFA082080UL, 0x82082080UL
};

/* next character without NUL character terminator */
uint32_t u8_nextmemchar(const char *s, size_t *i)
{
    uint32_t ch = 0;
    size_t sz = 0;
    do {
        ch <<= 6;
        ch += (unsigned char)s[(*i)++];
        sz++;
    } while (!isutf(s[*i]));
    ch -= offsetsFromUTF8[sz-1];

    return ch;
}

void u8_dec(const char *s, size_t *i)
{
    (void)(isutf(s[--(*i)]) || isutf(s[--(*i)]) || isutf(s[--(*i)]) || --(*i));
}

#define STB_TRUETYPE_IMPLEMENTATION 
#include "stb_truetype.h" /* http://nothings.org/stb/stb_truetype.h */

Pixmap ltk_draw_text(const unsigned char *text, int line_height, XColor color, XColor background)
{
    /* Get width of text */
    int w;
}

int main(int argc, const char * argv[])
{

    Display *display;
    int screen;
    Window window;
    GC gc;

    unsigned long black, white;
    Colormap colormap;
    display = XOpenDisplay((char *)0);
    screen = DefaultScreen(display);
    colormap = DefaultColormap(display, screen);
    black = BlackPixel(display, screen);
    white = WhitePixel(display, screen);
    window = XCreateSimpleWindow(display, DefaultRootWindow(display), 0, 0, 1366, 512, 0, black, white);
    XSetStandardProperties(display, window, "Random Window", NULL, None, NULL, 0, NULL);
    XSelectInput(display, window, ExposureMask|ButtonPressMask|KeyPressMask);
    gc = XCreateGC(display, window, 0, 0);
    XSetBackground(display, gc, black);
    XSetForeground(display, gc, black);
    XClearWindow(display, window);
    XMapRaised(display, window);

    /* load font file */
	FILE *f;
	long len;
	char *file_contents;
	f = fopen("font1.ttf", "rb");
	fseek(f, 0, SEEK_END);
	len = ftell(f);
	fseek(f, 0, SEEK_SET);
	file_contents = malloc(len + 1);
					fread(file_contents, 1, len, f);
					    file_contents[len] = '\0';
						fclose(f);
	unsigned char *fontBuffer = file_contents;
    
    /* prepare font */
    stbtt_fontinfo info;
    if (!stbtt_InitFont(&info, fontBuffer, 0))
    {
        printf("failed\n");
    }
    
    int b_w = 1366; /* bitmap width */
    int b_h = 100; /* bitmap height */
    int l_h = 100; /* line height */

    /* create a bitmap for the phrase */
    unsigned char *bitmap = malloc(b_w * b_h);
    
    /* calculate font scaling */
    float scale = stbtt_ScaleForPixelHeight(&info, l_h);

    //uint8_t *word = "ہمارے بارے میں";
    //char *word = "Hello";
    uint8_t *word = "sfsdf";
    
    int x = 0;
    
    int ascent, descent, lineGap;
    stbtt_GetFontVMetrics(&info, &ascent, &descent, &lineGap);
    
    ascent *= scale;
    descent *= scale;

    Pixmap pix = XCreatePixmap(display, window, b_w, l_h, 24);
    XColor green;
    XParseColor(display, colormap, "#00FFFF", &green);
    XAllocColor(display, colormap, &green);
    XColor green1;
    XParseColor(display, colormap, "#FF0000", &green1);
    XAllocColor(display, colormap, &green1);
    
    size_t i = 0;
    int length = strlen(word);
    uint32_t target;
    uint32_t target1;
    unsigned char *bob;
    int x1, y1, xoff, yoff;
    while (i < length)
    {
        target = u8_nextmemchar(word, &i);
        if (!target) break;
        /* get bounding box for character (may be offset to account for chars that dip above or below the line */
        int c_x1, c_y1, c_x2, c_y2;
        stbtt_GetCodepointBitmapBox(&info, target, scale, scale, &c_x1, &c_y1, &c_x2, &c_y2);
        
        /* compute y (different characters have different heights */
        int y = ascent + c_y1;
        
        /* render character (stride and offset is important here) */
        int byteOffset = x + (y  * b_w);
	bob = stbtt_GetCodepointBitmap(&info, scale, scale, target, &x1, &y1, &xoff, &yoff);
//        stbtt_MakeCodepointBitmap(&info, bitmap + byteOffset, c_x2 - c_x1, c_y2 - c_y1, b_w, scale, scale, target);
        
	    for (int i = 0; i < y1; i++)
	    {
		for (int j = 0; j < x1; j++)
		{
			XSetForeground(display, gc, (bob[i * x1 + j] / 255.0) * black + ((255 - bob[i * x1 + j]) / 255.0) * white);
			XDrawPoint(display, pix, gc, x + j, i);
		}
	    }
        
        /* how wide is this character */
        int ax;
        stbtt_GetCodepointHMetrics(&info, target, &ax, 0);
        x += ax * scale;

        /* add kerning */
        target1 = u8_nextmemchar(word, &i);
        if (!target1) break;
        u8_dec(word, &i);
        int kern;
        kern = stbtt_GetCodepointKernAdvance(&info, target, target1);
        x += kern * scale;
    }
    /* save out a 1 channel image */
    /*stbi_write_png("out.png", b_w, b_h, 1, bitmap, b_w);*/
    XSetForeground(display, gc, white);
    /*
    Pixmap pix = XCreatePixmapFromBitmapData(display, window, bitmap, b_w, b_h, white, black, 24);
    */
    XCopyArea(display, pix, window, gc, 0, 0, b_w, b_h, 0, 0);

    XEvent event;
    KeySym key;
    char text[255];

    while(1)
    {
        XNextEvent(display, &event);
        if (event.type == KeyPress && XLookupString(&event.xkey, text, 255, &key, 0) == 1)
        {
	    XCopyArea(display, pix, window, gc, 0, 0, b_w, b_h, 0, 0);
            if (text[0] == 'q')
            {
                XFreeGC(display, gc);
                XFreeColormap(display, colormap);
                XDestroyWindow(display, window);
                XCloseDisplay(display);
                exit(0);
            }
        }
    }
    
    free(fontBuffer);
    free(bitmap);
    
    return 0;
}
