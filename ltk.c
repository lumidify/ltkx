/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2016, 2017, 2018 lumidify <nobody@lumidify.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "text-hb.h"
#include "ltk.h"

void ltk_init(const char *theme_path)
{
	ltk_global = malloc(sizeof(Ltk));
	Ltk *ltk = ltk_global;	/* For convenience */
	ltk->display = XOpenDisplay(NULL);
	ltk->screen = DefaultScreen(ltk->display);
	ltk->colormap = DefaultColormap(ltk->display, ltk->screen);
	ltk->theme = ltk_load_theme(theme_path);
	ltk->window_hash = kh_init(winhash);
	ltk->wm_delete_msg = XInternAtom(ltk->display, "WM_DELETE_WINDOW", False);
	ltk->tm = ltk_init_text(ltk->theme->window->font);
}

void ltk_clean_up(void)
{
	LtkWindow *window;
	for (int k = kh_begin(ltk_global->window_hash); k != kh_end(ltk_global->window_hash); k++) {
		if (kh_exist(ltk_global->window_hash, k)) {
			ltk_destroy_window(kh_value(ltk_global->window_hash, k));
		}
	}
	kh_destroy(winhash, ltk_global->window_hash);
	XCloseDisplay(ltk_global->display);
	ltk_destroy_theme(ltk_global->theme);
	ltk_destroy_text_manager(ltk_global->tm);
	free(ltk_global);
}

void ltk_quit(void)
{
	ltk_clean_up();
	exit(0);
}

void ltk_fatal(const char *msg)
{
	fprintf(stderr, msg);
	ltk_clean_up();
	exit(1);
};

XColor ltk_create_xcolor(const char *hex)
{
	XColor color;
	XParseColor(ltk_global->display, ltk_global->colormap, hex,
		    &color);
	XAllocColor(ltk_global->display, ltk_global->colormap, &color);

	return color;
}

void ltk_mainloop(void)
{
	XEvent event;
	KeySym key;
	char text[255];

	while (1) {
		XNextEvent(ltk_global->display, &event);
		ltk_handle_event(event);
		/*
		   if (event.type == KeyPress && XLookupString(&event.xkey, text, 255, &key, 0) == 1)
		   {
		   if (text[0] == 'q')
		   {
		   XCloseDisplay(ltk_global->display);
		   exit(0);
		   }
		   }
		 */
	}
}

void ltk_redraw_window(LtkWindow * window)
{
	LtkWidget *ptr;
	if (!window) {
		return;
	}
	if (!window->root_widget) {
		return;
	}
	ptr = window->root_widget;
	ptr->draw(ptr);
}

LtkWindow *ltk_create_window(const char *title, int x, int y,
			     unsigned int w, unsigned int h)
{
	LtkWindow *window = malloc(sizeof(LtkWindow));
	if (!window)
		ltk_fatal("Not enough memory left for window!\n");
	LtkWindowTheme *wtheme = ltk_global->theme->window;	/* For convenience */
	Display *display = ltk_global->display;	/* For convenience */
	window->xwindow =
	    XCreateSimpleWindow(display, DefaultRootWindow(display), x, y,
				w, h, wtheme->border_width,
				wtheme->fg.pixel, wtheme->bg.pixel);
	window->gc = XCreateGC(display, window->xwindow, 0, 0);
	XSetForeground(display, window->gc, wtheme->fg.pixel);
	XSetBackground(display, window->gc, wtheme->bg.pixel);
	XSetStandardProperties(display, window->xwindow, title, NULL, None,
			       NULL, 0, NULL);
	XSetWMProtocols(display, window->xwindow,
			&ltk_global->wm_delete_msg, 1);
	window->root_widget = NULL;

	window->other_event = &ltk_window_other_event;

	window->rect.w = 0;
	window->rect.h = 0;
	window->rect.x = 0;
	window->rect.y = 0;

	XClearWindow(display, window->xwindow);
	XMapRaised(display, window->xwindow);
	XSelectInput(display, window->xwindow,
		     ExposureMask | KeyPressMask | KeyReleaseMask |
		     ButtonPressMask | ButtonReleaseMask |
		     StructureNotifyMask | PointerMotionMask);

	int ret;
	int k = kh_put(winhash, ltk_global->window_hash, window->xwindow, &ret);
	kh_value(ltk_global->window_hash, k) = window;
	ltk_global->window_num++;

	return window;
}

void ltk_remove_window(LtkWindow *window)
{
	ltk_destroy_window(window);
	if (ltk_global->window_num == 0)
		ltk_quit();
}

void ltk_destroy_window(LtkWindow * window)
{
	int k = kh_get(winhash, ltk_global->window_hash, window->xwindow);
	kh_del(winhash, ltk_global->window_hash, k);
	LtkWidget *ptr = window->root_widget;
	if (ptr)
		ptr->destroy(ptr);
	XDestroyWindow(ltk_global->display, window->xwindow);
	free(window);
	ltk_global->window_num--;
}

void ltk_window_other_event(void *widget, XEvent event)
{
	LtkWindow *window = widget;
	LtkWidget *ptr = window->root_widget;
	if (event.type == ConfigureNotify) {
		unsigned int w, h;
		w = event.xconfigure.width;
		h = event.xconfigure.height;
		if (ptr && ptr->resize
		    && (window->rect.w != w || window->rect.h != h)) {
			window->rect.w = w;
			window->rect.h = h;
			ptr->rect.w = w;
			ptr->rect.h = h;
			ptr->resize(ptr);
			ltk_redraw_window(window);
		}
	}
	if (event.type == Expose && event.xexpose.count == 0) {
		ltk_redraw_window(window);
	}
	if (event.type == ClientMessage
	    && event.xclient.data.l[0] == ltk_global->wm_delete_msg) {
		ltk_remove_window(window);
	}
}

void ltk_window_ini_handler(LtkTheme *theme, const char *prop, const char *value)
{
	if (strcmp(prop, "border_width") == 0) {
		theme->window->border_width = atoi(value);
	} else if (strcmp(prop, "bg") == 0) {
		theme->window->bg = ltk_create_xcolor(value);
	} else if (strcmp(prop, "fg") == 0) {
		theme->window->fg = ltk_create_xcolor(value);
	} else if (strcmp(prop, "font") == 0) {
		theme->window->font = strdup(value);
	}
}

int ltk_ini_handler(void *theme, const char *widget, const char *prop, const char *value)
{
	if (strcmp(widget, "window") == 0) {
		ltk_window_ini_handler(theme, prop, value);
	} else if (strcmp(widget, "button") == 0) {
		ltk_button_ini_handler(theme, prop, value);
	}
}

LtkTheme *ltk_load_theme(const char *path)
{
	LtkTheme *theme = malloc(sizeof(LtkTheme));
	theme->window = malloc(sizeof(LtkWindowTheme));
	theme->button = malloc(sizeof(LtkButtonTheme));
	if (ini_parse(path, ltk_ini_handler, theme) < 0) {
		fprintf(stderr, "ERROR: Can't load theme %s\n.", path);
		exit(1);
	}
	/*
	char *file_contents = ltk_read_file(path);

	cJSON *json = cJSON_Parse(file_contents);
	if (!json) {
		printf("Theme error before: [%s]\n", cJSON_GetErrorPtr());
		return NULL;
	}
	cJSON *button_json = cJSON_GetObjectItem(json, "button");
	if (!button_json) {
		printf("Theme error before: [%s]\n", cJSON_GetErrorPtr());
		return NULL;
	}
	cJSON *window_json = cJSON_GetObjectItem(json, "window");
	if (!window_json) {
		printf("Theme error before: [%s]\n", cJSON_GetErrorPtr());
		return NULL;
	}

	LtkTheme *theme = malloc(sizeof(LtkTheme));
	theme->button = ltk_parse_button_theme(button_json);
	theme->window = ltk_parse_window_theme(window_json);

	free(file_contents);
	cJSON_Delete(json);
	*/

	return theme;
}

void ltk_destroy_theme(LtkTheme * theme)
{
	free(theme->button);
	free(theme->window);
	free(theme);
}

char *ltk_read_file(const char *path, unsigned long *len)
{
	FILE *f;
	char *file_contents;
	f = fopen(path, "rb");
	fseek(f, 0, SEEK_END);
	*len = ftell(f);
	fseek(f, 0, SEEK_SET);
	file_contents = malloc(*len + 1);
	fread(file_contents, 1, *len, f);
	file_contents[*len] = '\0';
	fclose(f);

	return file_contents;
}

int ltk_collide_rect(LtkRect rect, int x, int y)
{
	return (rect.x <= x && (rect.x + rect.w) >= x && rect.y <= y
		&& (rect.y + rect.h) >= y);
}

void ltk_remove_active_widget(void *widget)
{
	if (!widget)
		return;
	LtkWidget *parent = widget;
	LtkWidget *child;
	while (parent->active_widget) {
		child = parent->active_widget;
		child->state = LTK_NORMAL;
		if (child->needs_redraw)
			child->draw(child);
		parent->active_widget = NULL;
		parent = child;
	}
}

void ltk_change_active_widget_state(void *widget, LtkWidgetState state)
{
	if (!widget)
		return;
	LtkWidget *ptr = widget;
	while (ptr = ptr->active_widget) {
		ptr->state = state;
		ptr->draw(ptr);
	}
}

void ltk_remove_hover_widget(void *widget)
{
	if (!widget)
		return;
	LtkWidget *parent = widget;
	LtkWidget *child;
	while (parent->hover_widget) {
		child = parent->hover_widget;
		child->state =
		    child->state ==
		    LTK_HOVERACTIVE ? LTK_ACTIVE : LTK_NORMAL;
		child->draw(child);
		parent->hover_widget = NULL;
		parent = child;
	}
}

LtkWidget ltk_create_widget(LtkWindow *window, void (*draw) (void *),
			    void (*destroy) (void *),
			    unsigned int needs_redraw)
{
	LtkWidget widget;
	widget.window = window;
	widget.active_widget = NULL;
	widget.hover_widget = NULL;
	widget.parent = NULL;

	widget.key_press = NULL;
	widget.key_release = NULL;
	widget.mouse_press = NULL;
	widget.mouse_release = NULL;
	widget.motion_notify = NULL;

	widget.resize = NULL;
	widget.draw = draw;
	widget.destroy = destroy;

	widget.needs_redraw = needs_redraw;
	widget.state = LTK_NORMAL;
	widget.row = 0;
	widget.rect.x = 0;
	widget.rect.y = 0;
	widget.rect.w = 100;
	widget.rect.h = 100;

	widget.row = NULL;
	widget.column = NULL;
	widget.row_span = NULL;
	widget.column_span = NULL;
	widget.sticky = 0;

	return widget;
}

void ltk_mouse_press_event(void *widget, XEvent event)
{
	LtkWidget *ptr = widget;
	if (!ptr || ptr->state == LTK_DISABLED)
		return;
	if (event.xbutton.button == 1) {
		LtkWidget *parent = ptr->parent;
		if (parent) {
			ltk_remove_active_widget(parent);
			parent->active_widget = ptr;
		}
		ptr->state = LTK_PRESSED;
		if (ptr->needs_redraw)
			ptr->draw(ptr);
	}
	if (ptr->mouse_press) {
		ptr->mouse_press(ptr, event);
	}
}

void ltk_mouse_release_event(void *widget, XEvent event)
{
	LtkWidget *ptr = widget;
	if (!ptr || ptr->state == LTK_DISABLED)
		return;
	if (ptr->state == LTK_PRESSED) {
		ptr->state = LTK_HOVERACTIVE;
		if (ptr->needs_redraw)
			ptr->draw(ptr);
	}
	if (ptr->mouse_release) {
		ptr->mouse_release(ptr, event);
	}
}

void ltk_motion_notify_event(void *widget, XEvent event)
{
	LtkWidget *ptr = widget;
	LtkWidget *parent;
	if (!ptr)
		return;
	short pressed = (event.xmotion.state & Button1Mask) == Button1Mask;
	if ((ptr->state == LTK_NORMAL || ptr->state == LTK_ACTIVE)
	    && !pressed) {
		ptr->state =
		    ptr->state == LTK_ACTIVE ? LTK_HOVERACTIVE : LTK_HOVER;
		parent = ptr->parent;
		if (parent) {
			ltk_remove_hover_widget(parent);
			parent->hover_widget = ptr;
		}
		if (ptr->needs_redraw)
			ptr->draw(ptr);
	}
	if (ptr->motion_notify)
		ptr->motion_notify(ptr, event);
}

void ltk_handle_event(XEvent event)
{
	LtkWindow *window;
	LtkWidget *root_widget;
	int k = kh_get(winhash, ltk_global->window_hash, event.xany.window);
	window = kh_value(ltk_global->window_hash, k);
	if (!window) return;
	root_widget = window->root_widget;
	switch (event.type) {
	case KeyPress:
		break;
	case KeyRelease:
		break;
	case ButtonPress:
		if (root_widget)
			ltk_mouse_press_event(root_widget, event);
		break;
	case ButtonRelease:
		if (root_widget)
			ltk_mouse_release_event(root_widget, event);
		break;
	case MotionNotify:
		if (root_widget)
			ltk_motion_notify_event(root_widget, event);
		break;
	default:
		/* FIXME: users should be able to register other events like closing the window */
		if (window->other_event)
			window->other_event(window, event);
	}
}
