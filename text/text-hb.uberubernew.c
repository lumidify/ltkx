/*
 * This file is part of the Lumidify ToolKit (LTK)
 * Copyright (c) 2017, 2018 lumidify <nobody@lumidify.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <harfbuzz/hb.h>
#include <harfbuzz/hb-ot.h>
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h" /* http://nothings.org/stb/stb_truetype.h */
#include "khash.h"

typedef struct {
	stbtt_fontinfo info;
	hb_font_t *hb;
	uint16_t id;
	unsigned int refs;
} LtkFont;

/* Contains general info on glyphs that doesn't change regardless of the context */
typedef struct _LtkGlyphInfo {
	unsigned char *alphamap;
	unsigned int w;
	unsigned int h;
	unsigned int xoff; /* x offset from origin to top left corner of glyph */
	unsigned int yoff; /* y offset from origin to top left corner of glyph */
	unsigned int refs;
	/* FIXME: does refs need to be long? It could cause problems if a
	program tries to cache/"keep alive" a lot of pages of text. */
} LtkGlyphInfo;

/* Contains glyph info specific to one run of text */
typedef struct _LtkGlyph {
	LtkGlyphInfo *info;
	int x_offset; /* additional x offset given by harfbuzz */
	int y_offset; /* additional y offset given by harfbuzz */
	int x_advance;
	int y_advance;
	uint32_t cluster; /* index of char in original text - from harfbuzz */
	struct _LtkGlyph *next;
} LtkGlyph;

typedef struct {
	unsigned int w;
	unsigned int h;
	int start_x;
	int start_y;
	char *str;
	LtkGlyph *start_glyph;
} LtkTextSegment;

/* Hash definitions */
/* glyph id -> glyph info struct */
KHASH_MAP_INIT_INT(glyphinfo, LtkGlyphInfo*)
/* font path, size -> glyph cache hash */
KHASH_MAP_INIT_INT(glyphcache, khash_t(glyphinfo)*)
/* font path -> font id */
KHASH_MAP_INIT_STR(fontid, uint16_t)
/* font id -> font struct */
KHASH_MAP_INIT_INT(fontstruct, LtkFont*)

typedef struct LtkTextManager_ {
	khash_t(fontid) *font_paths;
	khash_t(fontstruct) *font_cache;
	khash_t(glyphcache) *glyph_cache;
	uint16_t font_id_cur;
} LtkTextManager;

LtkTextManager *
ltk_init_text(void)
{
	LtkTextManager *tm = malloc(sizeof(LtkTextManager));
	if (!tm) {
		printf("Memory exhausted when trying to create text manager.");
		exit(1);
	}
	tm->font_paths = kh_init(fontid);
	tm->font_cache = kh_init(fontstruct);
	tm->glyph_cache = kh_init(glyphcache);
	tm->font_id_cur = 0;

	return tm;
}

LtkGlyphInfo *
ltk_create_glyph_info(LtkFont *font, unsigned int id, float scale)
{
	LtkGlyphInfo *glyph = malloc(sizeof(LtkGlyphInfo));
	if (!glyph) {
		printf("Out of memory!\n");
		exit(1);
	}
	
	glyph->alphamap = stbtt_GetGlyphBitmap(
		&font->info, scale, scale, id, &glyph->w,
		&glyph->h, &glyph->xoff, &glyph->yoff
	);

	return glyph;
}

LtkGlyphInfo *
ltk_get_glyph_info(LtkFont *font, unsigned int id, float scale, khash_t(glyphinfo) *cache)
{
	int ret;
	khint_t k;
	LtkGlyphInfo *glyph;
	k = kh_get(glyphinfo, cache, id);
	if (k == kh_end(cache)) {
		glyph = ltk_create_glyph_info(font, id, scale);
		glyph->refs = 0;
		/* FIXME: error checking with ret */
		k = kh_put(glyphinfo, cache, id, &ret);
		kh_value(cache, k) = glyph;
	} else {
		glyph = kh_value(cache, k);
		glyph->refs++;
	}

	return glyph;
}


khint_t
ltk_create_glyph_cache(LtkTextManager *tm, uint16_t font_id, uint16_t font_size)
{
	khash_t(glyphinfo) *cache = kh_init(glyphinfo);
	int ret;
	khint_t k;
	/* I guess I can just ignore ret for now */
	k = kh_put(glyphcache, tm->glyph_cache, font_id << 16 + font_size, &ret);
	kh_value(tm->glyph_cache, k) = cache;

	return k;
}

char *
ltk_load_file(const char *path, unsigned long *len)
{
	FILE *f;
	char *contents;
	f = fopen(path, "rb");
	fseek(f, 0, SEEK_END);
	*len = ftell(f);
	fseek(f, 0, SEEK_SET);
	contents = malloc(*len + 1);
	fread(contents, 1, *len, f);
	contents[*len] = '\0';
	fclose(f);
	return contents;
}

unsigned long
ltk_blend_pixel(Display *display, Colormap colormap, XColor fg, XColor bg, double a)
{
        if (a >= 1.0) {
		return fg.pixel;
        } else if (a == 0.0) {
		return bg.pixel;
	}

        XColor blended;
        blended.red = (int)((fg.red - bg.red) * a + bg.red);
        blended.green = (int)((fg.green - bg.green) * a + bg.green);
        blended.blue = (int)((fg.blue - bg.blue) * a + bg.blue);
        XAllocColor(display, colormap, &blended);

        return blended.pixel;
}

LtkFont *
ltk_create_font(char *path, uint16_t id)
{
	long len;
	LtkFont *font = malloc(sizeof(LtkFont));
	if (!font) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}
	char *contents = ltk_load_file(path, &len);
	if (!stbtt_InitFont(&font->info, contents, 0))
	{
		fprintf(stderr, "Failed to load font %s\n", path);
		exit(1);
	}
	/* FIXME: make use of the destroy function (last argument to hb_blob_create - see hb-blob.cc in harfbuzz source) */
	hb_blob_t *blob = hb_blob_create(contents, len, HB_MEMORY_MODE_READONLY, NULL, NULL);
	hb_face_t *face = hb_face_create(blob, 0);
	/* FIXME: need to use destroy function in order for the original file data to be freed? */
	hb_blob_destroy(blob);
	font->hb = hb_font_create(face);
	hb_face_destroy(face);
	hb_ot_font_set_funcs(font->hb);
	font->id = id;
	return font;
}

/* FIXME: need to figure out how exactly the whole font system is going to work, especially with default fonts, etc. */
uint16_t
ltk_load_font(LtkTextManager *tm, char *path)
{
	LtkFont *font = ltk_create_font(path, tm->font_id_cur++);
	int ret;
	khint_t k;
	k = kh_put(fontid, tm->font_paths, path, &ret);
	kh_value(tm->font_paths, k) = font->id;
	k = kh_put(fontstruct, tm->font_cache, (khint_t) font->id, &ret);
	kh_value(tm->font_cache, k) = font;

	return font->id;
}

uint16_t
ltk_get_font(LtkTextManager *tm, char *path)
{
	int ret;
	khint_t k;
	uint16_t id;
	k = kh_get(fontid, tm->font_paths, path);
	if (k == kh_end(tm->font_paths)) {
		id = ltk_load_font(tm, path);
	} else {
		id = kh_value(tm->font_paths, k);
	}

	return id;
}

/* FIXME: could use unsigned int for fontid and size as long as there is code to check neither of them become too large
   -> in case I want to get rid of uint_16_t, etc. */
LtkTextSegment *
ltk_create_text_segment(LtkTextManager *tm, char *text, uint16_t fontid, uint16_t size)
{
	/* (x1*, y1*): top left corner (relative to origin and absolute)
	   (x2*, y2*): bottom right corner (relative to origin and absolute) */
	LtkFont *font;
	khash_t(glyphinfo) *glyph_cache;
	khint_t k;

	k = kh_get(fontstruct, tm->font_cache, fontid);
	font = kh_value(tm->font_cache, k);
	/* FIXME: when should refs be increased? maybe only at the end in case
	   it has to return for some other reason (out of memory, etc.) */
	font->refs++;

	uint32_t attr = fontid << 16 + size;
	/* FIXME: turn this int ltk_get_glyph_cache */
	k = kh_get(glyphcache, tm->glyph_cache, attr);
	if (k == kh_end(tm->glyph_cache)) {
		k = ltk_create_glyph_cache(tm, fontid, size);
	}
	glyph_cache = kh_value(tm->glyph_cache, k);

	LtkTextSegment *ts = malloc(sizeof(LtkTextSegment));
	if (!ts) {
		fprintf(stderr, "Out of memory!\n");
		exit(1);
	}
	ts->str = text;

	hb_buffer_t *buf;
	hb_glyph_info_t *ginf, *gi;
	hb_glyph_position_t *gpos, *gp;
	unsigned int text_len = 0;
	int text_bytes = strlen(text);
	if (text_bytes < 1) {
		printf("WARNING: ltk_render_text_segment: length of text is less than 1.\n");
	}

	buf = hb_buffer_create();
	hb_buffer_set_flags(buf, HB_BUFFER_FLAG_BOT | HB_BUFFER_FLAG_EOT);
	hb_buffer_add_utf8(buf, text, text_bytes, 0, text_bytes);
	hb_buffer_guess_segment_properties(buf);
	hb_shape(font->hb, buf, NULL, 0);
	ginf = hb_buffer_get_glyph_infos(buf, &text_len);
	gpos = hb_buffer_get_glyph_positions(buf, &text_len);
	float scale = stbtt_ScaleForMappingEmToPixels(&font->info, size);
	LtkGlyph *last_glyph = NULL;

	int x_min = INT_MAX, x_max = INT_MIN, y_min = INT_MAX, y_max = INT_MIN;
	int x_abs = 0, y_abs = 0, x1_abs, y1_abs, x2_abs, y2_abs;
	for (int i = 0; i < text_len; i++) {
		gi = &ginf[i];
		gp = &gpos[i];
		LtkGlyph *glyph = malloc(sizeof(LtkGlyph));
		glyph->info = ltk_get_glyph_info(font, gi->codepoint, scale, glyph_cache);
		/* FIXME: round instead of just casting */
		glyph->x_offset = (int)(gp->x_offset * scale);
		glyph->y_offset = (int)(gp->y_offset * scale);
		glyph->x_advance = (int)(gp->x_advance * scale);
		glyph->y_advance = (int)(gp->y_advance * scale);
		glyph->next = NULL;
		if (i == 0) {
			ts->start_glyph = glyph;
		} else {
			last_glyph->next = glyph;
		}
		last_glyph = glyph;

		/* Calculate position in order to determine full size of text segment */
		x1_abs = x_abs + glyph->info->xoff + glyph->x_offset;
		y1_abs = y_abs + glyph->info->yoff - glyph->y_offset;
		x2_abs = x1_abs + glyph->info->w;
		y2_abs = y1_abs + glyph->info->h;
		if (x1_abs < x_min) x_min = x1_abs;
		if (y1_abs < y_min) y_min = y1_abs;
		if (x2_abs > x_max) x_max = x2_abs;
		if (y2_abs > y_max) y_max = y2_abs;
		x_abs += glyph->x_advance;
		y_abs -= glyph->y_advance;
	}
	/* FIXME: what was this supposed to do?
	   I think it was supposed to be the start drawing position, but why would this not be 0?
	   I'm guessing it had something to do with the fact that I need to calculate where the
	   actual top left corner of the glyph is since harfbuzz gives me the origin - maybe I
	   should just store that position directly in LtkGlyph? Is there any need to advance, etc.
	   later on after I've positioned the glyphs? Well, I guess I'll figure that out eventually... */
        ts->start_x = -x_min;
        ts->start_y = -y_min;
	ts->w = x_max - x_min;
	ts->h = y_max - y_min;
	return ts;
}

XImage *
ltk_render_text_segment(
	LtkTextSegment *ts,
	Display *dpy,
	Window window,
	GC gc,
	Colormap colormap,
	XColor fg,
	XColor bg)
{
	XWindowAttributes attrs;
	XGetWindowAttributes(dpy, window, &attrs);
	int depth = attrs.depth;
	XImage *img = XCreateImage(dpy, CopyFromParent, depth, ZPixmap, 0, NULL, ts->w, ts->h, 32, 0);
	img->data = calloc(img->bytes_per_line, img->height);
	XInitImage(img);
	int b;
	for (int i = 0; i < ts->h; i++) {
		b = img->bytes_per_line * i;
		for (int j = 0; j < ts->w; j++) {
			img->data[b++] = bg.blue / 257;
			img->data[b++] = bg.green / 257;
			img->data[b++] = bg.red / 257;
			b++;
		}
	}

	LtkGlyph *glyph = ts->start_glyph;
	int x_cur = ts->start_x;
	int y_cur = ts->start_y;
	int x, y;
	double a;
	unsigned int out_r, out_g, out_b;
	do {
		x = x_cur + glyph->info->xoff + glyph->x_offset;
		y = y_cur + glyph->info->yoff - glyph->y_offset;
		for (int i = 0; i < glyph->info->h; i++) {
			for (int j = 0; j < glyph->info->w; j++) {
				b = (y + i) * img->bytes_per_line + (x + j) * 4;
				a = glyph->info->alphamap[i * glyph->info->w + j] / 255.0;
				img->data[b] = (fg.blue * a + (1 - a) * (uint16_t)img->data[b] * 257) / 257;
				img->data[b + 1] = (fg.green * a + (1 - a) * (uint16_t)img->data[b + 1] * 257) / 257;
				img->data[b + 2] = (fg.red * a + (1 - a) * (uint16_t)img->data[b + 2] * 257) / 257;

				/*
				out_r = (fg.red / 255) * a1 + (uint8_t)img->data[b + 2] * (255 - a1);
				out_g = (fg.green / 255) * a1 + (uint8_t)img->data[b + 1] * (255 - a1);
				out_b = (fg.blue / 255) * a1 + (uint8_t)img->data[b] * (255 - a1);
				out_r = (out_r + 1 + (out_r >> 8)) >> 8;
				out_g = (out_g + 1 + (out_g >> 8)) >> 8;
				out_b = (out_b + 1 + (out_b >> 8)) >> 8;
				*/
			}
		}
		x_cur += glyph->x_advance;
		y_cur -= glyph->y_advance;
	} while (glyph = glyph->next);

	return img;
}

int main(int argc, char *argv[])
{
    Display *display;
    int screen;
    Window window;
    GC gc;

    unsigned long black, white;
    Colormap colormap;
    display = XOpenDisplay((char *)0);
    screen = DefaultScreen(display);
    colormap = DefaultColormap(display, screen);
    black = BlackPixel(display, screen);
    white = WhitePixel(display, screen);
    XSetWindowAttributes wattr;
    wattr.background_pixel = white;
    wattr.border_pixel = black;
    wattr.colormap = colormap;
    window = XCreateWindow(display, DefaultRootWindow(display), 0, 0, 1366, 512, 0, 24, CopyFromParent, CopyFromParent, CWBackPixel | CWBorderPixel, &wattr);
    XSetStandardProperties(display, window, "Random Window", NULL, None, NULL, 0, NULL);
    XSelectInput(display, window, ExposureMask|ButtonPressMask|KeyPressMask);
    gc = XCreateGC(display, window, 0, 0);
    XSetBackground(display, gc, black);
    XSetForeground(display, gc, black);
    XClearWindow(display, window);
    XMapRaised(display, window);
    XColor c1, c2;
    XParseColor(display, colormap, "#FFFFFF", &c1);
    XParseColor(display, colormap, "#FF0000", &c2);
    XAllocColor(display, colormap, &c1);
    XAllocColor(display, colormap, &c2);

    LtkTextManager *tm = ltk_init_text();
    uint16_t font_id = ltk_get_font(tm, "NotoNastaliqUrdu-Regular.ttf");
    uint16_t font_size = 100;
    LtkTextSegment *text = ltk_create_text_segment(tm, "ہمارے بارے میں", font_id, font_size);
    //Pixmap pix = ltk_render_text_segment(text, display, window, gc, colormap, c1, c2);
    XImage *img = ltk_render_text_segment(text, display, window, gc, colormap, c1, c2);
    //XCopyArea(display, pix, window, gc, 0, 0, text->w, text->h, 0, 0);
    XPutImage(display, window, gc, img, 0, 0, 0, 0, text->w, text->h);

    XEvent event;
    KeySym key;
    char txt[255];

    while(1)
    {
        XNextEvent(display, &event);
        if (event.type == KeyPress && XLookupString(&event.xkey, txt, 255, &key, 0) == 1)
        {
            //XCopyArea(display, pix, window, gc, 0, 0, text->w, text->h, 0, 0);
	    XPutImage(display, window, gc, img, 0, 0, 0, 0, text->w, text->h);
            if (txt[0] == 'q')
            {
                XFreeGC(display, gc);
                XFreeColormap(display, colormap);
                XDestroyWindow(display, window);
                XCloseDisplay(display);
                exit(0);
            }
        }
    }
    
    return 0;
}
